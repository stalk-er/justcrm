-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 
-- Версия на сървъра: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `just_crm`
--

-- --------------------------------------------------------

--
-- Структура на таблица `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `admins`
--

INSERT INTO `admins` (`id`, `email`, `password`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `remember_token`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
(1, 'admin@change.me', '$2y$10$eLz1.4nr8N3zBkk.g19OeOyGowcHpqUbaf1EBshWbyCNI/NInk2mG', NULL, 0, NULL, NULL, NULL, NULL, NULL, '7U48srsu08PdHWwJcxvm4ySst7afiJ7uaZGiElx2FhnbIKWugkgEj68dxKbq', 'Tsvetan', 'Dimitrov', '2017-05-02 16:09:00', '2017-05-02 16:10:10');

-- --------------------------------------------------------

--
-- Структура на таблица `admin_role`
--

CREATE TABLE `admin_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `admin_role`
--

INSERT INTO `admin_role` (`role_id`, `admin_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Структура на таблица `attachments`
--

CREATE TABLE `attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `calender_events`
--

CREATE TABLE `calender_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `end` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `allDay` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `calender_events`
--

INSERT INTO `calender_events` (`id`, `title`, `description`, `start`, `end`, `allDay`, `created_at`, `updated_at`) VALUES
(1, 'Da ta iba w gaza', 'Tapakkk', 'Mon May 01 2017 12:00:00 GMT+0300', 'Invalid date', 0, '2017-05-29 17:16:46', '2017-05-29 17:16:46'),
(2, 'Голям си тапак', 'въй въй :Д', '2017-05-29T02:30:00+03:00', '2017-05-29T09:00:00+03:00', 0, '2017-05-29 17:17:57', '2017-05-29 17:18:08');

-- --------------------------------------------------------

--
-- Структура на таблица `contracts`
--

CREATE TABLE `contracts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `start_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` int(10) UNSIGNED NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `contract_templates`
--

CREATE TABLE `contract_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `overview` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `events`
--

INSERT INTO `events` (`id`, `title`, `overview`, `content`, `status`, `author`, `action`, `created_at`, `updated_at`) VALUES
(9, 'Product minimum quantity.', 'Products reached their minimum quantity stock.', 'The following products just reached the minimum quantity stock: Тенджера,Черен тигам,Тоалетна хартия', '1', NULL, '/products/store', '2017-06-13 15:25:36', '2017-06-13 15:29:42');

-- --------------------------------------------------------

--
-- Структура на таблица `event_comment`
--

CREATE TABLE `event_comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `event_comment_attachments`
--

CREATE TABLE `event_comment_attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment_id` int(10) UNSIGNED NOT NULL,
  `attachment_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `unique_id` int(11) NOT NULL,
  `template` int(10) UNSIGNED NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `invoice_templates`
--

CREATE TABLE `invoice_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `item_types`
--

CREATE TABLE `item_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `links`
--

CREATE TABLE `links` (
  `id` int(10) UNSIGNED NOT NULL,
  `display` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `main` tinyint(1) DEFAULT NULL,
  `show_menu` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `links`
--

INSERT INTO `links` (`id`, `display`, `url`, `created_at`, `updated_at`, `main`, `show_menu`) VALUES
(1, 'Links', 'Link', '2017-05-02 16:09:00', '2017-05-02 16:09:01', 1, 1),
(2, 'Roles', 'Role', NULL, NULL, 1, 1),
(3, 'Permissions', 'Permission', NULL, NULL, 1, 1),
(4, 'Admins', 'Admin', NULL, '2017-05-02 16:09:01', 1, 1),
(6, 'Products', 'products', '2017-05-02 16:14:43', '2017-05-13 13:59:04', NULL, 1),
(7, 'Vendors', 'vendors', '2017-05-02 16:15:20', '2017-05-02 16:15:35', NULL, 1),
(8, 'Contracts', 'contracts', '2017-05-02 16:15:59', '2017-05-02 16:23:33', NULL, 1),
(9, 'Events', 'events', '2017-05-02 16:16:07', '2017-05-02 16:23:26', NULL, 1),
(10, 'Invoices', 'invoices', '2017-05-02 16:16:27', '2017-05-02 16:23:20', NULL, 1),
(11, 'Orders', 'orders', '2017-05-02 16:16:36', '2017-05-15 16:59:18', NULL, 1),
(12, 'Organizations', 'organizations', '2017-05-02 16:17:00', '2017-05-02 16:23:07', NULL, 1),
(13, 'Quotes', 'quotes', '2017-05-02 16:17:18', '2017-05-02 16:22:58', NULL, 1),
(14, 'Services', 'services', '2017-05-02 16:17:23', '2017-05-02 16:22:50', NULL, 1),
(15, 'Payment Types', 'payment_types', '2017-05-02 16:17:45', '2017-05-02 16:22:26', NULL, 1),
(16, 'Order Statuses', 'order_statuses', '2017-05-02 17:17:27', '2017-05-02 17:19:38', NULL, 1),
(17, 'Shipment Methods', 'shipment_types', '2017-05-02 17:18:46', '2017-05-02 17:20:03', NULL, 1),
(18, 'product_statusess', 'product_statuses', '2017-05-13 18:09:39', '2017-05-13 18:09:39', NULL, 1);

-- --------------------------------------------------------

--
-- Структура на таблица `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_07_30_165435_create_settings_table', 1);

-- --------------------------------------------------------

--
-- Структура на таблица `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `unique_id` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipment_types` int(10) UNSIGNED DEFAULT NULL,
  `payment_types` int(10) UNSIGNED DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(10) UNSIGNED DEFAULT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `orders`
--

INSERT INTO `orders` (`id`, `unique_id`, `description`, `shipment_types`, `payment_types`, `address`, `client`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 322087, '<p>Пешо си купува кола.</p> ', 1, 1, 'ул. Карандила номер 7', 'Pesho', 1, 'Tsvetan', '2017-05-02 17:59:03', '2017-07-29 10:30:53'),
(2, 528150, 'На Асен поръчката бе.', 4, 2, 'На едно място в планината', 'Асен', 1, 'cvetan', '2017-05-15 17:54:26', '2017-05-15 17:54:26'),
(3, 561941, 'blablablaaa', 4, 2, 'Sliven, ul. Klucohor 4', 'Dob4o', 2, 'cvetan', '2017-05-29 16:46:54', '2017-05-29 16:46:54'),
(4, 414360, 'adsasdasdas', 4, 1, 'asdasdasdas', 'dasdasd', 3, 'cvetan', '2017-06-11 16:33:44', '2017-06-11 16:33:44'),
(5, 359862, 'asdasd', 4, 1, 'asdas', 'asdasdasd', 5, 'cvetan', '2017-06-11 16:37:15', '2017-06-11 16:37:15'),
(6, 140767, 'sda', 1, 1, 'dasda', 'sdas', 3, 'cvetan', '2017-06-11 16:37:58', '2017-06-11 16:37:58'),
(7, 909528, 'sda', 1, 1, 'dasda', 'sdas', 3, 'cvetan', '2017-06-11 16:38:36', '2017-06-11 16:38:36'),
(8, 945234, 'sda', 1, 1, 'dasda', 'sdas', 3, 'cvetan', '2017-06-11 16:38:56', '2017-06-11 16:38:56'),
(9, 176649, 'sda', 1, 1, 'dasda', 'sdas', 3, 'cvetan', '2017-06-11 16:39:14', '2017-06-11 16:39:14'),
(10, 157104, 'sda асдасдасдасдасдасдасд', 1, 2, 'dasdaасдасдасдасдасдасдасдасдасдасд', 'sdas ', 3, 'cvetan', '2017-06-11 16:39:28', '2017-07-26 17:36:36'),
(11, 905581, 'sda', 1, 1, 'dasda', 'sdas', 3, 'cvetan', '2017-06-11 16:39:42', '2017-06-11 16:39:42'),
(12, 904622, 'sda', 1, 1, 'dasda', 'sdas', 3, 'cvetan', '2017-06-11 16:40:28', '2017-06-11 16:40:28'),
(13, 809164, 'sda', 1, 1, 'dasda', 'sdas', 3, 'cvetan', '2017-06-11 16:40:48', '2017-06-11 16:40:48'),
(14, 915208, 'asdaasdas', 3, 2, 'asdasd', 'dasdasdasdasdasd', 3, 'cvetan', '2017-06-11 16:43:12', '2017-06-11 16:43:12'),
(15, 853137, 'aaaaaaaaaaa', 3, 2, 'aaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaa', 2, 'cvetan', '2017-06-11 16:45:33', '2017-06-11 16:45:33'),
(16, 697822, 'a', 5, 1, 'aaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 2, 'cvetan', '2017-06-11 16:46:38', '2017-06-11 16:46:38'),
(17, 895359, 'dasdasdasdasdasd', 4, 2, 'dadddddd', 'asdasdas', 1, 'cvetan', '2017-06-11 16:52:35', '2017-06-11 16:52:35'),
(18, 538111, 'vvvvvvvv', 3, 2, 'aaaaaaaaaaa', 'asdasd', 3, 'cvetan', '2017-06-11 16:55:17', '2017-06-11 16:55:17'),
(19, 353588, 'aaaaaaaa', 3, 3, 'aaaaaaaaaaaaaaaaaa', 'dasdasdas', 1, 'cvetan', '2017-06-11 16:57:18', '2017-06-11 16:57:18'),
(20, 519710, 'asdas', 4, 1, 'dasd', 'dasdasdas', 1, 'cvetan', '2017-06-11 16:58:42', '2017-06-11 16:58:42'),
(21, 860504, 'dasdasdasd', 3, 1, 'asdasd', 'asdasdasdasd', 1, 'cvetan', '2017-06-11 17:01:09', '2017-06-11 17:01:09'),
(22, 238114, 'adas', 3, 2, 'dasdasdas', 'asdasdasd', 2, 'cvetan', '2017-06-11 17:08:46', '2017-06-11 17:08:46'),
(23, 484094, 'sdasdasd', 1, 1, 'asdasdasda', 'asdasdasdasd', 1, 'cvetan', '2017-06-12 15:16:46', '2017-06-12 15:16:46'),
(24, 240095, 'dasdasdadasdasdasdasdasd', 2, 3, 'asda', 'dasdasdas', 1, 'cvetan', '2017-06-12 16:09:11', '2017-06-12 16:09:11'),
(25, 205558, 'asdasdasdasdasd', 3, 2, 'asdasd', 'dasdasdasdasd', 4, 'cvetan', '2017-06-13 15:01:43', '2017-06-13 15:01:43'),
(26, 558611, 'asdasdasdasdasd', 3, 2, 'asdasd', 'dasdasdasdasd', 4, 'cvetan', '2017-06-13 15:02:20', '2017-06-13 15:02:20'),
(27, 925534, 'asdasd', 5, 3, 'asdasd', 'aasdasdasdasdasdasd', 4, 'cvetan', '2017-06-13 15:07:35', '2017-06-13 15:07:35'),
(28, 626321, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 3, 2, 'bbbbbbbbbbbbbbbbbbbbbb', 'ccccccccccccccccccccccccccc', 3, 'cvetan', '2017-06-13 15:10:57', '2017-06-13 15:10:57'),
(29, 727719, 'dasda', 4, 1, 'sdasd', 'dasdasdasdas', 3, 'cvetan', '2017-06-13 15:14:53', '2017-06-13 15:14:53'),
(30, 854991, 'asdas', 3, 2, 'dasdas', 'asdasdasdasdasdasd', 3, 'cvetan', '2017-06-13 15:25:36', '2017-06-13 15:25:36'),
(31, 221236, '$validator$validator$validator', 4, 1, '$validator$validator$validator', '$validator$validator$validator', 1, 'cvetan', '2017-07-23 16:18:12', '2017-07-23 16:18:12'),
(32, 381116, '$request->input("items")$request->input("items")$request->input("items")$request->input("items")', 4, 1, '$request->input("items")$request->input("items")$request->input("items")', '$request->input("items")', 1, 'cvetan', '2017-07-23 16:24:04', '2017-07-23 16:24:04'),
(33, 326042, '$request->input("items")$request->input("items")$request->input("items")$request->input("items")', 4, 1, '$request->input("items")$request->input("items")$request->input("items")', '$request->input("items")', 1, 'cvetan', '2017-07-23 16:32:05', '2017-07-23 16:32:05'),
(34, 880706, '$request->input("items")$request->input("items")$request->input("items")$request->input("items")', 4, 1, '$request->input("items")$request->input("items")$request->input("items")', '$request->input("items")', 1, 'cvetan', '2017-07-23 16:32:12', '2017-07-23 16:32:12'),
(48, 647005, 'asdaasdasdasdasda', 4, 1, 'sdasdasdasdasdasda', 'asdasdasdasdasdasdasdasd', 1, 'cvetan', '2017-07-23 16:43:09', '2017-07-23 16:43:09'),
(59, 963077, 'sddddddddddddddddadasdasd', 4, 1, 'asdddddddddddddddddddddddddd', 'asdasda', 1, 'cvetan', '2017-07-23 16:53:56', '2017-07-23 16:53:56'),
(60, 664696, 'sddddddddddddddddadasdasd', 4, 1, 'asdddddddddddddddddddddddddd', 'asdasda', 1, 'cvetan', '2017-07-23 16:57:06', '2017-07-23 16:57:06'),
(61, 541417, 'sddddddddddddddddadasdasd', 4, 1, 'asdddddddddddddddddddddddddd', 'asdasda', 1, 'cvetan', '2017-07-23 16:59:28', '2017-07-23 16:59:28'),
(62, 697690, 'sddddddddddddddddadasdasd', 4, 1, 'asdddddddddddddddddddddddddd', 'asdasda', 1, 'cvetan', '2017-07-23 17:00:12', '2017-07-23 17:00:12'),
(63, 363758, 'IVAILO E TAPAK', 4, 1, 'IVAILO E TAPAK', 'IVAILO E TAPAK', 1, 'cvetan', '2017-07-23 17:01:11', '2017-07-23 17:01:11'),
(64, 394482, 'IVAILO E TAPAK', 4, 1, 'IVAILO E TAPAK', 'IVAILO E TAPAK', 1, 'cvetan', '2017-07-23 17:02:26', '2017-07-23 17:02:26'),
(65, 156982, 'IVAILO E TAPAK', 4, 1, 'IVAILO E TAPAK', 'IVAILO E TAPAK', 1, 'cvetan', '2017-07-23 17:02:30', '2017-07-23 17:02:30'),
(66, 504540, 'NOVA PORUCHKAABbbbbb', 3, 2, 'NOVA PORUCHKAaaa', 'Normalen klient', 3, 'cvetan', '2017-07-23 17:03:32', '2017-07-24 16:32:54');

-- --------------------------------------------------------

--
-- Структура на таблица `order_product`
--

CREATE TABLE `order_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `name`, `price`, `quantity`, `total`, `created_at`, `updated_at`) VALUES
(1, 3, 4, NULL, 2.00, 0, 0.00, '2017-05-29 16:46:54', '2017-05-29 16:46:54'),
(2, 4, 4, NULL, 2.00, 0, 0.00, '2017-06-11 16:33:44', '2017-06-11 16:33:44'),
(3, 4, 5, NULL, 1.34, 0, 0.00, '2017-06-11 16:33:44', '2017-06-11 16:33:44'),
(4, 4, 6, NULL, 3.14, 0, 0.00, '2017-06-11 16:33:44', '2017-06-11 16:33:44'),
(5, 5, 5, NULL, 1.34, 0, 0.00, '2017-06-11 16:37:15', '2017-06-11 16:37:15'),
(6, 6, 5, NULL, 2.00, 5, 10.00, '2017-06-11 16:37:58', '2017-07-24 15:57:45'),
(7, 7, 5, NULL, 2.00, 3, 6.00, '2017-06-11 16:38:36', '2017-07-24 15:57:09'),
(8, 8, 5, NULL, 1.34, 0, 0.00, '2017-06-11 16:38:56', '2017-06-11 16:38:56'),
(9, 9, 5, NULL, 1.34, 0, 0.00, '2017-06-11 16:39:14', '2017-06-11 16:39:14'),
(10, 10, 5, NULL, 7.00, 3, 21.00, '2017-06-11 16:39:28', '2017-07-26 17:37:03'),
(11, 11, 5, NULL, 1.34, 0, 0.00, '2017-06-11 16:39:42', '2017-06-11 16:39:42'),
(12, 12, 5, NULL, 1.34, 0, 0.00, '2017-06-11 16:40:28', '2017-06-11 16:40:28'),
(13, 13, 5, NULL, 1.34, 0, 0.00, '2017-06-11 16:40:48', '2017-06-11 16:40:48'),
(14, 14, 4, NULL, 2.00, 0, 0.00, '2017-06-11 16:43:12', '2017-06-11 16:43:12'),
(15, 14, 6, NULL, 3.14, 0, 0.00, '2017-06-11 16:43:13', '2017-06-11 16:43:13'),
(16, 15, 5, NULL, 1.34, 0, 0.00, '2017-06-11 16:45:33', '2017-06-11 16:45:33'),
(17, 16, 5, NULL, 1.34, 5, 6.70, '2017-06-11 16:46:38', '2017-06-11 16:46:38'),
(18, 16, 4, NULL, 2.00, 5, 10.00, '2017-06-11 16:46:38', '2017-06-11 16:46:38'),
(19, 17, 4, NULL, 2.00, 32, 64.00, '2017-06-11 16:52:35', '2017-06-11 16:52:35'),
(20, 18, 4, NULL, 2.00, 32, 64.00, '2017-06-11 16:55:17', '2017-06-11 16:55:17'),
(21, 19, 5, NULL, 1.34, 32, 42.88, '2017-06-11 16:57:18', '2017-06-11 16:57:18'),
(22, 20, 5, NULL, 1.34, 123, 164.82, '2017-06-11 16:58:42', '2017-06-11 16:58:42'),
(23, 20, 6, NULL, 3.14, 0, 0.00, '2017-06-11 16:58:42', '2017-06-11 16:58:42'),
(24, 21, 6, NULL, 3.14, 2, 6.28, '2017-06-11 17:01:09', '2017-06-11 17:01:09'),
(25, 22, 6, NULL, 3.14, 5, 15.70, '2017-06-11 17:08:46', '2017-06-11 17:08:46'),
(26, 23, 6, NULL, 3.14, 0, 0.00, '2017-06-12 15:16:46', '2017-06-12 15:16:46'),
(27, 24, 6, NULL, 3.14, 32, 100.48, '2017-06-12 16:09:11', '2017-06-12 16:09:11'),
(28, 25, 4, NULL, 2.00, 12, 24.00, '2017-06-13 15:01:43', '2017-06-13 15:01:43'),
(29, 26, 4, NULL, 2.00, 12, 24.00, '2017-06-13 15:02:20', '2017-06-13 15:02:20'),
(30, 26, 5, NULL, 1.34, 0, 0.00, '2017-06-13 15:02:20', '2017-06-13 15:02:20'),
(31, 27, 4, NULL, 1.00, 12, 12.00, '2017-06-13 15:07:35', '2017-06-13 15:07:35'),
(32, 27, 6, NULL, 3.14, 0, 0.00, '2017-06-13 15:07:35', '2017-06-13 15:07:35'),
(33, 28, 4, NULL, 2.00, 60, 120.00, '2017-06-13 15:10:57', '2017-06-13 15:10:57'),
(34, 28, 5, NULL, 1.34, 0, 0.00, '2017-06-13 15:10:57', '2017-06-13 15:10:57'),
(35, 29, 5, NULL, 1.34, 25, 33.50, '2017-06-13 15:14:53', '2017-06-13 15:14:53'),
(36, 29, 6, NULL, 3.14, 0, 0.00, '2017-06-13 15:14:53', '2017-06-13 15:14:53'),
(37, 30, 6, NULL, 3.14, 64, 200.96, '2017-06-13 15:25:36', '2017-06-13 15:25:36'),
(40, 62, 4, NULL, 2.00, 32, 64.00, '2017-07-23 17:00:12', '2017-07-23 17:00:12'),
(41, 63, 5, NULL, 1.34, 4, 5.36, '2017-07-23 17:01:11', '2017-07-23 17:01:11'),
(42, 64, 5, NULL, 1.34, 4, 5.36, '2017-07-23 17:02:26', '2017-07-23 17:02:26'),
(43, 65, 5, NULL, 1.34, 4, 5.36, '2017-07-23 17:02:30', '2017-07-23 17:02:30'),
(44, 66, 5, NULL, 3.14, 3, 9.42, '2017-07-23 17:03:32', '2017-07-24 16:42:39'),
(45, 66, 4, NULL, 2.67, 4, 10.68, '2017-07-23 17:03:32', '2017-07-24 16:58:50'),
(46, 66, 7, 'Some name', 1.09, 5, 5.45, '2017-07-23 17:03:32', '2017-07-24 16:58:50'),
(56, 66, 6, NULL, 0.97, 6, 5.82, '2017-07-24 16:31:51', '2017-07-24 16:58:50'),
(57, 10, 6, NULL, 3.00, 2, 6.00, '2017-07-26 17:28:20', '2017-07-26 17:28:20'),
(58, 1, 5, NULL, 1.34, 3, 4.02, '2017-07-29 10:30:53', '2017-07-29 10:30:53');

-- --------------------------------------------------------

--
-- Структура на таблица `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Pending', '2017-05-02 17:22:44', '2017-05-02 17:22:44'),
(2, 'Awaiting', '2017-05-02 17:22:55', '2017-05-02 17:22:55'),
(3, 'Sent', '2017-05-02 17:23:17', '2017-05-02 17:23:17'),
(4, 'Traveling', '2017-05-02 17:23:36', '2017-05-02 17:23:36'),
(5, 'Not In Stock', '2017-05-02 17:24:04', '2017-05-02 17:24:04');

-- --------------------------------------------------------

--
-- Структура на таблица `organizations`
--

CREATE TABLE `organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` int(11) DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_lon` double(8,2) DEFAULT NULL,
  `location_lat` double(8,2) DEFAULT NULL,
  `member_of` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employees` int(11) NOT NULL DEFAULT '0',
  `annual_revenue` double(8,2) DEFAULT '0.00',
  `type` int(10) UNSIGNED NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `organization_types`
--

CREATE TABLE `organization_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `password_reminders`
--

CREATE TABLE `password_reminders` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `payment_types`
--

CREATE TABLE `payment_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `payment_types`
--

INSERT INTO `payment_types` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Master Card', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt aliquam lacus in lacinia. Interdum et malesuada fames ac ante ipsum primis in faucibus.', '2017-05-02 16:32:46', '2017-05-02 16:32:46'),
(2, 'Visa', '<p><span style="font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></p>', '2017-05-02 16:35:21', '2017-05-02 16:35:21'),
(3, 'Maestro', '<p><span style="font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></p>', '2017-05-02 16:35:50', '2017-05-02 16:35:50');

-- --------------------------------------------------------

--
-- Структура на таблица `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `end_price` double(8,2) DEFAULT '0.00',
  `b2b_price` double(8,2) DEFAULT '0.00',
  `vendor_price` double(8,2) DEFAULT '0.00',
  `vendor` int(10) UNSIGNED DEFAULT NULL,
  `status` int(10) UNSIGNED DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  `lot` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiry_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `barcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `products`
--

INSERT INTO `products` (`id`, `name`, `avatar`, `description`, `end_price`, `b2b_price`, `vendor_price`, `vendor`, `status`, `quantity`, `lot`, `delivery_date`, `expiry_date`, `created_by`, `created_at`, `updated_at`, `barcode`) VALUES
(4, 'Тенджера', 'ugc\\crash_twinsanity___crash_bandicoot_by_postmortacum-d9n4nx3.png', 'Тенджера', 2.00, 3.00, 1.00, 1, 3, 76, '20312312', '08/24/0005', '08/18/0005', 'cvetan', '2017-05-14 08:56:58', '2017-06-13 15:10:57', '5201410003055'),
(5, 'Черен тигам', 'ugc\\redpanda-sleeping.jpg', 'Голям боклук', 1.34, 2.15, 3.10, 4, 3, 34, '125812367347345', '05/31/2017', '05/31/2017', 'cvetan', '2017-05-29 18:56:39', '2017-06-13 15:14:53', '5201410003055'),
(6, 'Тоалетна хартия', 'ugc\\12728-red-panda-1920x1200-animal-wallpaper-19ju24o.jpg', 'Мекичка тоалетна хартия', 3.14, 3.21, 1.11, 4, 3, 500, '5568592434561', '05/31/2017', '06/01/2017', 'cvetan', '2017-05-29 18:58:05', '2017-06-13 15:25:36', '5038483163603'),
(7, 'Бира', 'ugc\\redpanda-sleeping.jpg', '123123', 12.00, 21.00, 23.00, 4, 1, 5, '', '07/27/2017', '07/28/2017', 'cvetan', '2017-07-19 17:05:00', '2017-07-19 17:05:00', '3123123123123');

-- --------------------------------------------------------

--
-- Структура на таблица `product_statuses`
--

CREATE TABLE `product_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `product_statuses`
--

INSERT INTO `product_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Available', '2017-05-14 08:46:06', '2017-05-14 08:46:06'),
(2, 'In Stock', '2017-05-14 08:46:31', '2017-05-14 08:46:31'),
(3, 'Not In Stock', '2017-05-14 08:46:41', '2017-05-14 08:46:41'),
(4, 'Pending', '2017-05-14 08:46:54', '2017-05-14 08:46:54'),
(5, 'Delivering', '2017-05-14 08:48:35', '2017-05-14 08:48:35');

-- --------------------------------------------------------

--
-- Структура на таблица `quotes`
--

CREATE TABLE `quotes` (
  `id` int(10) UNSIGNED NOT NULL,
  `unique_id` text CHARACTER SET utf8 NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` int(10) UNSIGNED NOT NULL,
  `shipment_type` int(10) UNSIGNED DEFAULT NULL,
  `quote_items` longtext CHARACTER SET utf8,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `quotes`
--

INSERT INTO `quotes` (`id`, `unique_id`, `name`, `content`, `client`, `template`, `shipment_type`, `quote_items`, `created_by`, `created_at`, `updated_at`) VALUES
(84, '451234', 'Оферта за болница Майчин дом', 'Оферта за майчин дом по повод Търг за набиране на посуда.\r\nВъй, въй.\r\n:D\r\n', 'Майчин Дом', 2, 4, '[{"id":"4","name":"\\u0422\\u0435\\u043d\\u0434\\u0436\\u0435\\u0440\\u0430","quantity":"26","unit_price":"2.18"},{"id":"4","name":"\\u0422\\u0435\\u043d\\u0434\\u0436\\u0435\\u0440\\u0430","quantity":"50","unit_price":"3.69"},{"id":"4","name":"\\u0422\\u0435\\u043d\\u0434\\u0436\\u0435\\u0440\\u0430","quantity":"4","unit_price":"2.67"},{"id":"4","name":"\\u0422\\u0435\\u043d\\u0434\\u0436\\u0435\\u0440\\u0430","quantity":"11","unit_price":"2.3"},{"id":"4","name":"\\u0422\\u0435\\u043d\\u0434\\u0436\\u0435\\u0440\\u0430","quantity":"4","unit_price":"2"},{"id":"5","name":"\\u0427\\u0435\\u0440\\u0435\\u043d \\u0442\\u0438\\u0433\\u0430\\u043c","quantity":"4","unit_price":"1.34"}]', 'cvetan', '2017-05-24 14:44:54', '2017-07-29 14:00:31'),
(85, '844671', 'aaaaaaaaaaaaaaaaaa', 'асдасдасдасдасдасдасдасда\r\nсд\r\nа\r\nсд\r\nас\r\nд\r\nа\r\nсд\r\nа\r\nсд', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1, 3, '[{"id":"4","name":"\\u0422\\u0435\\u043d\\u0434\\u0436\\u0435\\u0440\\u0430","quantity":"4","unit_price":"3.2"}]', 'cvetan', '2017-07-27 16:45:25', '2017-07-27 16:54:55'),
(86, '383752', 'asd123asd123asd123asd123', 'асдасдасдасдасдасдасд асдасдасдасдасдасдасдасд асдасдасдасдасдасдасдасд асдасдасдасдасдасдасдасд асдасдасдасдасдасдасдасд асдасдасдасдасдасдасдасд асдасдасдасдасдасдасдасд асдасдасдасдасдасда', 'asd123asd123asd123', 1, 3, '[{"id":"7","name":"\\u0411\\u0438\\u0440\\u0430","quantity":"3","unit_price":"12.12"},{"id":"4","name":"\\u0422\\u0435\\u043d\\u0434\\u0436\\u0435\\u0440\\u0430","quantity":"2","unit_price":"2.2"},{"id":"5","name":"\\u0427\\u0435\\u0440\\u0435\\u043d \\u0442\\u0438\\u0433\\u0430\\u043c","quantity":"3","unit_price":"1.34"}]', 'cvetan', '2017-07-27 16:57:08', '2017-07-27 16:58:54');

-- --------------------------------------------------------

--
-- Структура на таблица `quotes_items`
--

CREATE TABLE `quotes_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `quote_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `shipment_type_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit_price` decimal(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `quote_templates`
--

CREATE TABLE `quote_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `settings` text CHARACTER SET utf8,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `quote_templates`
--

INSERT INTO `quote_templates` (`id`, `name`, `settings`, `created_at`, `updated_at`) VALUES
(1, 'Service Quote template', '{"itemsCount":"0","name":"Service Quote template","title_position":"left","from_position":"second","to_position":"second","slogan_position":"right","comments_show":"on","unique_number_show":"on"}', '2017-05-17 21:00:00', '2017-07-30 14:12:57'),
(2, 'Product Quote template', NULL, '2017-05-17 21:00:00', '2017-05-17 21:00:00');

-- --------------------------------------------------------

--
-- Структура на таблица `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(1, 'super', 'This goup has all permissions', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) DEFAULT NULL,
  `execution_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `services`
--

INSERT INTO `services` (`id`, `name`, `description`, `price`, `execution_time`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Почистване', 'Почистване на стъкла', 50, '3', '', '2017-05-17 17:50:04', '2017-05-17 17:50:04'),
(2, 'асдасд', 'асдасдасд', 312, '31', '', '2017-07-27 17:00:56', '2017-07-27 17:00:56');

-- --------------------------------------------------------

--
-- Структура на таблица `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 'company_logo', 'ugc\\crash_twinsanity___crash_bandicoot_by_postmortacum-d9n4nx3.png', '2017-07-30 14:55:37', '2017-07-30 15:06:57'),
(2, 'company_address', 'Main office:\r\nSofia 1766, Mladost 4, Ring road 265 (across "The Bells")', '2017-07-30 14:55:37', '2017-07-30 15:06:57'),
(3, 'company_telephone', '+358 40 513 3225\r\n070012340\r\neshop@mail.technopolis.bg', '2017-07-30 14:55:37', '2017-07-30 15:06:57'),
(4, 'company_slogan', 'In code we trust', '2017-07-30 14:55:37', '2017-07-30 15:06:57');

-- --------------------------------------------------------

--
-- Структура на таблица `shipment_types`
--

CREATE TABLE `shipment_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `shipment_types`
--

INSERT INTO `shipment_types` (`id`, `name`, `description`, `price`, `created_at`, `updated_at`) VALUES
(1, 'By Air', '<p><span style="font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></p>', 3.34, '2017-05-02 17:27:46', '2017-05-02 17:27:46'),
(2, 'By Water', '<p><span style="font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></p>', 3.5, '2017-05-02 17:28:05', '2017-05-02 17:28:05'),
(3, 'Under Water', '<p><span style="font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></p>', 2.87, '2017-05-02 17:28:16', '2017-05-02 17:28:16'),
(4, 'Teleportation', '<p><span style="font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></p>', 8.93, '2017-05-02 17:28:28', '2017-05-02 17:28:28'),
(5, 'By automobile', '<p><span style="font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span></p>', 1.4, '2017-05-02 17:28:57', '2017-05-02 17:28:57');

-- --------------------------------------------------------

--
-- Структура на таблица `system_messages`
--

CREATE TABLE `system_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `unique_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `system_settings`
--

CREATE TABLE `system_settings` (
  `settings` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `profile_image`, `password`, `confirmed`, `confirmation_code`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'cvetan', 'dimitrovovich@gmail.com', '/avatars/default.png', '$2y$10$.Etoapd2F36xoFqeWy17uO2xb7D1Rc7/KeAG5Q5dcewPXzDejcOFy', 1, NULL, 'Ghxum0TxISFMZ9BqiMVDLUewe5BURA3GHv245fx0c21IezdXSBaYuU5XrjDX', '2017-05-15 17:52:10', '2017-07-30 14:29:53');

-- --------------------------------------------------------

--
-- Структура на таблица `vendors`
--

CREATE TABLE `vendors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` int(11) NOT NULL,
  `fax` int(11) NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_lon` double(8,2) DEFAULT NULL,
  `location_lat` double(8,2) DEFAULT NULL,
  `avatar_image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Схема на данните от таблица `vendors`
--

INSERT INTO `vendors` (`id`, `name`, `description`, `city`, `country`, `address`, `zip_code`, `email`, `telephone`, `fax`, `website`, `location_lon`, `location_lat`, `avatar_image`, `created_at`, `updated_at`) VALUES
(1, 'Маратонки ООД', 'асл плс', 'София', 'България', 'ASasasdasdasdas', 1092, 'info@maratonki.com', 988924287, 293785, 'http://maratonki.bg', 23.24, 43.23, 'ugc\\crash_twinsanity___crash_bandicoot_by_postmortacum-d9n4nx3.png', '2017-05-13 12:34:11', '2017-07-27 15:06:45'),
(4, 'Някакво нормално име', 'Някакво нормално описание', 'София', 'България', 'ул. Каспичан Номер 9', 6666, 'normalize@this.shit', 987924287, 976315236, 'http://www.normalize.com/', NULL, NULL, 'ugc\\redpanda-sleeping.jpg', '2017-05-17 14:40:52', '2017-05-29 18:51:08'),
(5, 'Свястно име', 'Свясно', 'Свестен', 'България', 'ул. Свясна работа Номер 1', 1111, 'good@job.com', 988924287, 2147483647, 'http://www.goodboy.com/', NULL, NULL, 'ugc\\redpanda-sleeping.jpg', '2017-05-17 14:43:05', '2017-05-29 18:52:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD KEY `admins_activation_code_index` (`activation_code`),
  ADD KEY `admins_reset_password_code_index` (`reset_password_code`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`role_id`,`admin_id`),
  ADD KEY `admin_role_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calender_events`
--
ALTER TABLE `calender_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contracts_template_foreign` (`template`);

--
-- Indexes for table `contract_templates`
--
ALTER TABLE `contract_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_comment`
--
ALTER TABLE `event_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_comment_author_id_foreign` (`author_id`),
  ADD KEY `event_comment_event_id_foreign` (`event_id`);

--
-- Indexes for table `event_comment_attachments`
--
ALTER TABLE `event_comment_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_comment_attachments_comment_id_foreign` (`comment_id`),
  ADD KEY `event_comment_attachments_attachment_id_foreign` (`attachment_id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_template_foreign` (`template`);

--
-- Indexes for table `invoice_templates`
--
ALTER TABLE `invoice_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_types`
--
ALTER TABLE `item_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_shipment_types_foreign` (`shipment_types`),
  ADD KEY `orders_payment_types_foreign` (`payment_types`),
  ADD KEY `orders_status_foreign` (`status`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_product_order_id_foreign` (`order_id`),
  ADD KEY `order_product_product_id_foreign` (`product_id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organizations_type_foreign` (`type`);

--
-- Indexes for table `organization_types`
--
ALTER TABLE `organization_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reminders`
--
ALTER TABLE `password_reminders`
  ADD KEY `password_reminders_email_index` (`email`),
  ADD KEY `password_reminders_token_index` (`token`);

--
-- Indexes for table `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_vendor_foreign` (`vendor`),
  ADD KEY `products_status_foreign` (`status`);

--
-- Indexes for table `product_statuses`
--
ALTER TABLE `product_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quotes_template_foreign` (`template`);

--
-- Indexes for table `quotes_items`
--
ALTER TABLE `quotes_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quotes_items_quote_id_foreign` (`quote_id`),
  ADD KEY `quotes_items_product_id_foreign` (`product_id`),
  ADD KEY `quotes_items_shipment_type_id_foreign` (`shipment_type_id`);

--
-- Indexes for table `quote_templates`
--
ALTER TABLE `quote_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipment_types`
--
ALTER TABLE `shipment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_messages`
--
ALTER TABLE `system_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `calender_events`
--
ALTER TABLE `calender_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contracts`
--
ALTER TABLE `contracts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contract_templates`
--
ALTER TABLE `contract_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `event_comment`
--
ALTER TABLE `event_comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_comment_attachments`
--
ALTER TABLE `event_comment_attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice_templates`
--
ALTER TABLE `invoice_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item_types`
--
ALTER TABLE `item_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `organization_types`
--
ALTER TABLE `organization_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `product_statuses`
--
ALTER TABLE `product_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `quotes_items`
--
ALTER TABLE `quotes_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quote_templates`
--
ALTER TABLE `quote_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `shipment_types`
--
ALTER TABLE `shipment_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `system_messages`
--
ALTER TABLE `system_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Ограничения за дъмпнати таблици
--

--
-- Ограничения за таблица `admin_role`
--
ALTER TABLE `admin_role`
  ADD CONSTRAINT `admin_role_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `admin_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `contracts`
--
ALTER TABLE `contracts`
  ADD CONSTRAINT `contracts_template_foreign` FOREIGN KEY (`template`) REFERENCES `contract_templates` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `event_comment`
--
ALTER TABLE `event_comment`
  ADD CONSTRAINT `event_comment_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `event_comment_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `event_comment_attachments`
--
ALTER TABLE `event_comment_attachments`
  ADD CONSTRAINT `event_comment_attachments_attachment_id_foreign` FOREIGN KEY (`attachment_id`) REFERENCES `attachments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `event_comment_attachments_comment_id_foreign` FOREIGN KEY (`comment_id`) REFERENCES `event_comment` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_template_foreign` FOREIGN KEY (`template`) REFERENCES `invoice_templates` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_payment_types_foreign` FOREIGN KEY (`payment_types`) REFERENCES `payment_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_shipment_types_foreign` FOREIGN KEY (`shipment_types`) REFERENCES `shipment_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_status_foreign` FOREIGN KEY (`status`) REFERENCES `order_statuses` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `order_product_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_type_foreign` FOREIGN KEY (`type`) REFERENCES `organization_types` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_status_foreign` FOREIGN KEY (`status`) REFERENCES `product_statuses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_vendor_foreign` FOREIGN KEY (`vendor`) REFERENCES `vendors` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `quotes`
--
ALTER TABLE `quotes`
  ADD CONSTRAINT `quotes_template_foreign` FOREIGN KEY (`template`) REFERENCES `quote_templates` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `quotes_items`
--
ALTER TABLE `quotes_items`
  ADD CONSTRAINT `quotes_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `quotes_items_quote_id_foreign` FOREIGN KEY (`quote_id`) REFERENCES `quotes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `quotes_items_shipment_type_id_foreign` FOREIGN KEY (`shipment_type_id`) REFERENCES `shipment_types` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
