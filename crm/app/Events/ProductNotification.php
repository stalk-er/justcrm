<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Auth;

class ProductNotification
{
    use InteractsWithSockets, SerializesModels;

    /**
    * Create a new event instance.
    *
    * @return void
    */
    public function __construct()
    {
        $products = \App\Product::where("quantity", "<", 20);
        if($products->exists()) {
            $event = new \App\Event();
            $productNames = [];
            foreach($products->get() as $product) {
                array_push($productNames, $product->name);
            }
            $event->create([
                "title" => "Product minimum quantity.",
                "overview" => "Products reached their minimum quantity stock.",
                "content" => "The following products just reached the minimum quantity stock: " . join(",", $productNames),
                "status" => 0,
                "author" => Auth::user()->name,
                "action" => "/products/store"
            ]);
        }
    }

    /**
    * Get the channels the event should broadcast on.
    *
    * @return Channel|array
    */
    public function broadcastOn()
    {
        return ["ProductNotificationChanel"];
    }
}
