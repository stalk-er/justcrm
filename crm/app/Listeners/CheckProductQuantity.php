<?php

namespace App\Listeners;

use App\Events\ProductNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CheckProductQuantity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductNotification  $event
     * @return void
     */
    public function handle(ProductNotification $event)
    {
        //
    }
}
