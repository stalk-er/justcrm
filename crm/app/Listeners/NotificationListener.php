<?php

namespace App\Listeners;

use App\Events\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationListener
{

    CONST EVENT = 'score.update';
    CONST CHANNEL = 'score.update';

    /**
    * Create the event listener.
    *
    * @return void
    */
    public function __construct()
    {
        //
    }

    /**
    * Handle the event.
    *
    * @param  Notification  $event
    * @return void
    */
    public function handle(Notification $event)
    {
        $redis = Redis::connection();
        $redis->publish(self::CHANNEL, $data);
    }
}
