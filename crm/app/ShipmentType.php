<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentType extends Model
{
    protected $table = 'shipment_types';
    protected $fillable = [
        
        "name",
        "description"
        
    ];
    
}
