<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Auth;

class PartialComposerServiceProvider extends ServiceProvider {

    /**
    * Bootstrap the application services.
    *
    * @return void
    */
    public function boot() {

        $this->notificationMenuData();
    }

    /**
    * Register the application services.
    *
    * @return void
    */
    public function register() {
        //
    }

    public function notificationMenuData() {

        return view()->composer("partials.notificationMenu", function($view) {

            $notifications = \App\Event::where("status", "=", "0")->take(10)->get();

            $view->with('notifications', $notifications);
        });
    }
}
