<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemMessage extends Model
{
    
    protected $table = "system_messages";
    protected $fillable = [
        "unique_id"
    ];
}
