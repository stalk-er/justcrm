<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStatus extends Model {

    protected $table = "product_statuses";
    protected $fillable = [
        "name"
    ];

    public function products() {

        return $this->hasMany("\App\Product", "status")->get();
    }

}
