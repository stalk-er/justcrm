<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $fillable = [
        
        "unique_id",
        "template",
        "created_by"
        
    ];
    
    public function order(){
        
        $this->belongsTo('\App\Order');
    }
}
