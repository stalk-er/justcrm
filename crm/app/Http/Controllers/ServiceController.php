<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;

class ServiceController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        // List all services
        return view("services.service-list")->with("services", \App\Service::paginate(10));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view("services.service-actions");
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $rules = array(
            'name'       => 'required',
            'description'      => 'max:200',
            'price' => 'numeric',
            'execution_time' => 'required',
        );

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return redirect()
            ->back()
            ->withErrors($validator)
            ->withInput();
        }

        $service = new \App\Service();
        $request->input()["created_by"] = Auth::user()->username;
        if($service->create($request->input()))
        return redirect("services");
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        return view("services.service-actions")->with("service", \App\Service::find($id));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $service = \App\Service::find($id);
        $rules = array(
                'name' => 'required',
                'description'      => 'max:200',
                'price' => 'numeric',
                'execution_time' => 'required',
            );

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return redirect()
            ->back()
            ->withErrors($validator)
            ->withInput();
        }

        $request->input()["created_by"] = Auth::user()->username;
        if($service->update($request->input()))
        return redirect("services");
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        if(\App\Service::find($id)->delete())
            return redirect("services");
    }
}
