<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginController
 *
 * @author Mimas
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller {

    // Return login page
    public function getLogin() {


        if (Auth::check()) {
            return view('dashboard');
        } else {
            return view('login');
        }
    }

    public function postLogin(Request $request) {

        $this->validate($request, [
            'login-username' => 'required',
            'login-password' => 'required'
        ]);
        $username = $request->input('login-username');
        $password = $request->input('login-password');

        $credentials = [
            'username' => $username,
            'password' => $password,
            'confirmed' => 1
        ];

        if (!Auth::attempt($credentials)) {

            return back()->withErrors("Wrong Credentials. Please try again");
        }

        return redirect()->intended('dashboard');
    }

    // Logout
    public function getLogout() {
        Auth::logout();
        return redirect('login');
    }

}
