<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SystemSettingsController extends Controller
{
    public function edit()
    {
        return view("system-settings.edit")->with("settings", \App\SystemSettings::all());
    }
}
