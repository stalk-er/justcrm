<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Input;

class QuoteController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view("quotes.quote-list")->with("quotes", \App\Quote::paginate(10));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $quoteTemplates = [];
        $qt = \App\QuoteTemplate::all();
        foreach($qt as $q) {
            $quoteTemplates[$q->id] = $q->name;
        }
        return view("quotes.quote-actions")
                ->with("quoteTemplates", $quoteTemplates)
                ->with("shipmentTypes", \App\ShipmentType::all());
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'content' => 'required|min:10',
            'client' => 'required|min:3'
        );

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return redirect()
            ->back()
            ->withErrors($validator)
            ->withInput();
        }

        $quote = new \App\Quote();
        $inputAll = $request->input();
        $inputAll["created_by"] = Auth::user()->username;
        $inputAll["quote_items"] = json_encode($request->input("items"));
        $inputAll["unique_id"] = (string) mt_rand(100001, 999998);

        if($quote->create($inputAll)) {
            return redirect("quotes");
        }
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $quote = \App\Quote::find($id);
        return view("quotes.quote")
                ->with("quote", $quote->with("shipmentType")->first())
                ->with("template", $quote->qTemplate)
                ->with("quoteItems", json_decode($quote->quote_items));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $quoteTemplates = [];
        $qt = \App\QuoteTemplate::all();
        foreach($qt as $q)
            $quoteTemplates[$q->id] = $q->name;

        $quote = \App\Quote::find($id);

        return view("quotes.quote-actions")
        ->with("quote", $quote)
        ->with("template", $quote->qTemplate)
        ->with("quoteTemplates", $quoteTemplates)
        ->with("quoteItems", json_decode($quote->quote_items))
        ->with("shipmentTypes", \App\ShipmentType::all());
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $quote = \App\Quote::find($id);
        $rules = array(
            'name' => 'required',
            'content' => 'required|min:10',
            'client' => 'required|min:3'
        );

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $inputAll = $request->input();
        $inputAll["created_by"] = Auth::user()->username;
        $inputAll["quote_items"] = json_encode($request->input("items"));


        if($quote->update($inputAll))
            return redirect("quotes");
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        if(\App\Quote::find($id)->delete())
            return redirect("quotes");
    }
}
