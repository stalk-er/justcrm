<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use Bican\Roles\Models\Role;
use App\User;
use DB;

class EventController extends Controller {

    public function getCalendar() {

        return view("calendar.calendar");
    }

    public function getEvents() {

        return view("calendar.event-list")
                    ->with("events", \App\Event::all());
    }

    public function getEventDetails($id) {

        return view("calendar.event-details")
                        ->with("event", \App\Event::find($id))
                        ->with("event_activity", \App\Event::find($id)->activity);
    }

    public function saveComment(Request $request) {

        $event_comment = new \App\EventComment();
        $event_comment->content = $request->data["content"];
        $event_comment->author_id = $request->data["author"];
        $event_comment->event_id = $request->data["event"];
        $event_comment->save();
    }

    public function fetchComments(Request $request) {

        $eid = $request->data_refresh["event_id"];
        $comments = \App\Event::find($eid)->comments();
        $lid = $comments->first()->id;
        if (intval($request->data_refresh["last_comment_id"]) !== $lid) {
            $index = array_search($lid, $comments);
            unset($comments[$index]);
            return response()->json($comments);
        }
    }

    public function fetchCalendarEvents() {

        // TODO - fetch only the current user events on the calendar
        $calenderEvents = \App\CalenderEvents::all();
        foreach($calenderEvents as $ce) {
            if($ce->allDay == 1)
            $ce->allDay = true;
            else
            $ce->allDay = false;
        }
        return response()->json(
            $calenderEvents
        );
    }

    public function deleteCalendarEvent(Request $request)
    {
        $eventId = $request["id"];
        \App\CalenderEvents::find($eventId)->delete();
    }

    public function saveCalendarEvent(Request $request) {

        $event = new \App\CalenderEvents();
        $event->title = $request["event"]["title"];
        $event->description = $request["event"]["description"];
        $event->start = $request["event"]["start"];
        $event->end = $request["event"]["end"];
        $event->save();
        return $event;
    }

    public function updateCalendarEvent(Request $request) {

        $event = \App\CalenderEvents::find($request["event"]["id"]);
        $event->title = $request["event"]["title"];
        $event->description = $request["event"]["description"];
        $event->start = $request["event"]["start"];
        $event->end = $request["event"]["end"];
        $event->save();
        return $event;
    }
}
