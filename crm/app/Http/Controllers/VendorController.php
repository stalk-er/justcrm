<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // List all vendors
        return view("vendors.vendor-list")->with("vendors", \App\Vendor::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("vendors.vendor-actions");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
         $rules = array(
             'name'       => 'required',
             'description'      => 'max:200',
             'city' => 'max:20',
             'country' => 'max:100',
             'address' => 'max:100',
             'zip_code' => 'numeric',
             'email' => 'email',
             'telephone' => 'numeric:12',
             'fax' => 'numeric',
             'website' => 'max:100',
         );

         $validator = Validator::make($request->input(), $rules);
         if ($validator->fails()) {
             return redirect()
             ->back()
             ->withErrors($validator)
             ->withInput();
         }

         $vendor = new \App\Vendor();
         if($vendor->create($request->input()))
         return redirect("vendors");
     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id)
     {
         //
     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
         return view("vendors.vendor-actions")->with("vendor", \App\Vendor::find($id));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
         //
         $vendor = \App\Vendor::find($id);
         $rules = array(
             'name'       => 'required',
             'description'      => 'max:200',
             'city' => 'max:20',
             'country' => 'max:100',
             'address' => 'max:100',
             'zip_code' => 'numeric',
             'email' => 'email',
             'telephone' => 'numeric:12',
             'fax' => 'numeric',
             'website' => 'max:100',
         );

         $validator = Validator::make($request->input(), $rules);
         if ($validator->fails()) {
             return redirect()
             ->back()
             ->withErrors($validator)
             ->withInput();
         }

         if($vendor->update($request->input()))
         return redirect("vendors");
     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(\App\Vendor::find($id)->delete())
            return redirect("vendors");
    }
}
