<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationEventController extends Controller
{
    public function markRead(Request $request)
    {
        $notificationId = (int) $request["notificationId"];

        $notification = \App\Event::find($notificationId);
        $notification->update([
            "status" => 1
        ]);

        return "status changed";
    }
}
