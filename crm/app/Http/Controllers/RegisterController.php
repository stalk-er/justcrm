<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginController
 *
 * @author Mimas
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Input;
use Validator;
use Redirect;
use Hash;
use Mail;

class RegisterController extends Controller {

    public function postRegister(Request $request) {

       
        $confirmation_code = str_random(30);
        $user = new User;
        $user->username = $request->input('register-username');
        $user->email = $request->input('register-email');
        $user->profile_image = "/avatars/default.jpg";
        $user->password = Hash::make($request->input('register-password'));
        $user->confirmation_code = $confirmation_code;
        $user->save();
        
        Mail::send('mail.welcome', ["confirmation_code" => $confirmation_code], function($message) {
            $message->to(Input::get('register-email'), Input::get('register-username'))
                    ->subject('Verify your email address');
        });

        return redirect('login')->with('Thanks for signing up! Please check your email.');
    }

    public function confirm($confirmation_code) {

        if (!$confirmation_code) {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::where('confirmation_code', $confirmation_code)->first();

        if (!$user) {
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        return redirect('login')->with('You have successfully verified your account.');
    }

}
