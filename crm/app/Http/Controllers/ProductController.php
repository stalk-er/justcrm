<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use Bican\Roles\Models\Role;
use App\User;

class ProductController extends Controller {

    public function getProducts() {
        $products = \App\Product::all();
        return view("products.product-list")
                        ->with("products", $products);
    }

    public function createProduct(Request $request) {

        $product = new \App\Product();
        $product->name = $request->input("product-name");
        $product->description = $request->input("product-description");
        $product->end_price = $request->input("product-endprice");
        $product->b2b_price = $request->input("product-b2bprice");
        $product->vendor_price = $request->input("product-vendor-price");
        $product->vendor = $request->input("product-vendor");
        $product->status = $request->input("product-status");
        $product->quantity = $request->input("product-quantity");
        $product->lot = $request->input("product-lot");
        $product->barcode = $request->input("product-barcode");
        $product->avatar = $request->input("avatar");
        $product->delivery_date = $request->input("product-delivery-date");
        $product->expiry_date = $request->input("product-expiry-date");
        $product->created_by = Auth::user()->username;

        if ($product->save()) {
            return redirect("products/list");
        }
    }

    public function editProduct(Request $request, $id) {

        $product = \App\Product::find($id);
        $product->name = $request->input("product-name");
        $product->description = $request->input("product-description");
        $product->end_price = $request->input("product-endprice");
        $product->b2b_price = $request->input("product-b2bprice");
        $product->vendor_price = $request->input("product-vendor-price");
        $product->vendor = $request->input("product-vendor");
        $product->status = $request->input("product-status");
        $product->quantity = $request->input("product-quantity");
        $product->lot = $request->input("product-lot");
        $product->barcode = $request->input("product-barcode");
        $product->avatar = $request->input("avatar");
        $product->delivery_date = $request->input("product-delivery-date");
        $product->expiry_date = $request->input("product-expiry-date");
        $product->created_by = Auth::user()->username;

        event(new App\Events\ProductNotification());

        if ($product->save()) {
            return redirect("products/list");
        }
    }

    public function deleteProduct($id) {

        $product = \App\Product::find($id);
        $product->delete();
    }

    public function getSingleProduct($id) {

        $single_product = \App\Product::find($id);

        return $single_product;
    }

    public function getProductDetails($id) {

        $product = \App\Product::find($id);
        return response()->json($product);
    }

    public function storeExistingProduct()
    {
        return view("products.storeProduct")
        ->with("product_statuses", \App\ProductStatus::all());
    }

    public function getSingleProductByBarcode($barcode)
    {
        $product = \App\Product::where("barcode", "=", $barcode);

        if($product->exists()) {
            $product = $product->first();
            return response()->json([
                "success" => $product
            ]);
        }

        return response()->json([
            "error" => "No such barcode in the database"
        ]);
    }

    public function storeSingleProductByBarcode($barcode, Request $request)
    {

        $productQuantity = \App\Product::where("barcode", "=", $barcode)->firstOrFail()->quantity;
        $product = \App\Product::where("barcode", "=", $barcode)->update([
            "lot" => $request["product-lot"],
            "barcode" => $request["product-barcode"],
            "delivery_date" => $request["product-delivery-date"],
            "expiry_date" => $request["product-expiry-date"],
            "status" => $request["product-status"],
            "quantity" => (int)$request["product-quantity"] + $productQuantity
        ]);

        if($product)
            return response()->json([
                "success" => "Product successfully stored!"
            ]);
        else
            return response()->json([
                "error" => "Unable to save the product!"
            ]);
    }

    public function getProductsAsJson(Request $request)
    {

        $products = \App\Product::where("name", "LIKE", "%".$request["term"]."%")->get();
        foreach($products as $product) {
            $product->value = $product->name;
            $product->label = $product->name;
            $product->id = $product->id;
            $product->name = $product->name;
            $product->text = $product->description;
        }
        return response()->json(
            $products
        );
    }

    public function getProductByTerm(Request $request)
    {
        $term = $request["term"];
        return \App\Product::where("name", "LIKE", "%$term%")->first();
    }
}
