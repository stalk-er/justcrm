<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SystemMessage extends Controller {

    public function __construct() {
        
    }

    public function pushToEventList($title, $content, $status, $author) {

        $event = new \App\Event();
        $event->title = $title;
        $event->overview = "This is a system message.";
        $event->status = $status;
        $event->author = $author;
        $event->save();
    }
}
