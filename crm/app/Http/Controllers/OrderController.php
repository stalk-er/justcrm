<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Input;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = \App\Order::paginate(10);

        return view("orders.order-list")->with("orders", $orders);
    }

    public function dashboard()
    {
        return view("orders.dashboard")->with("statuses", \App\OrderStatus::all());
    }

    public function ordersDoughnut()
    {
        // get data for order statistics
        $orders = \App\Order::with("status")->orderBy("status")->get();

        return response()->json($orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stock_products = \App\Product::take(3)->get();
        $services = \App\Service::all();
        return view("orders.order-actions")
            ->with("stock_products", $stock_products)
            ->with("services", $services)
            ->with("shipment_types", \App\ShipmentType::all())
            ->with("payment_types", \App\PaymentType::all())
            ->with("order_statuses", \App\OrderStatus::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'description' => 'min:10',
            'client' => 'required|min:3',
            'shipment_types' => 'required',
            'payment_types' => 'required',
            'address' => 'required|min:3',
        );

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return redirect()
            ->back()
            ->withErrors($validator)
            ->withInput();
        }

        $inputAll = $request->input();
        $inputAll["created_by"] = Auth::user()->username;
        $inputAll["unique_id"] = (string) mt_rand(100001, 999998);

        if($order = \App\Order::create($inputAll)) {

            foreach ($request->input("items") as $key => $value) {

                $op = new \App\OrderProduct();
                $op->order_id = $order->id;
                $op->product_id = (int) $value["id"];
                $op->price = (float) $value["unit_price"];
                $op->quantity = (int) $value["quantity"];
                $op->total = (int) $value["quantity"] * (float) $value["unit_price"];
                $op->save();

            }

            return redirect("orders");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
         $order = \App\Order::find($id);
         $ordered_products = $order->products()->get();
         $stock_products = \App\Product::take(3)->get();
         $total = 5;

         return view("orders.order-actions")
             ->with("order", $order)
             ->with("stock_products", $stock_products)
             ->with("products", $ordered_products)
             ->with("order_statuses", \App\OrderStatus::all())
             ->with("shipment_types", \App\ShipmentType::all())
             ->with("payment_types", \App\PaymentType::all())
             ->with("total", $total);
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $order = \App\Order::find($id);
        $rules = array(
            'description' => 'min:10',
            'client' => 'required|min:3',
            'shipment_types' => 'required',
            'payment_types' => 'required',
            'address' => 'required|min:3',
        );

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $inputAll = $request->input();
        $inputAll["created_by"] = $order->created_by;

        $order->update($inputAll);

        if($order) {

            $items = $inputAll["items"];
            foreach($items as $key => $value) {

                if(!isset($value["order_product_id"]))
                    $value["order_product_id"] = 0;

                $value["price"] = $value["unit_price"];
                unset($value["unit_price"]);
                $value["total"] = (int) $value["quantity"] * (float) $value["price"];
                $value["order_id"] = $order->id;

                $orderProduct = \App\OrderProduct::find($value["order_product_id"]); // Find by order product id

                if($orderProduct != null) {
                    $orderProduct->quantity = $value["quantity"];
                    $orderProduct->total = $value["total"];
                    $orderProduct->price = $value["price"];
                    $orderProduct->save();
                }
                else {

                    $orderProduct = new \App\OrderProduct();
                    $orderProduct->quantity = $value["quantity"];
                    $orderProduct->price = $value["price"];
                    $orderProduct->product_id = $value["id"];
                    $orderProduct->order_id = $value["order_id"];
                    $orderProduct->total = $value["total"];
                    $orderProduct->save();
                }
            }

            return redirect("orders");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(\App\Order::destroy($id))
            return redirect("orders");
    }
}
