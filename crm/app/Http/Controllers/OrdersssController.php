<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use Bican\Roles\Models\Role;
use App\User;
use App\Events\ProductNotification;

class OrderController extends Controller {

    public function getOrders() {

        $orders = \App\Order::all();
        $new_orders = \App\Order::where("status", 1)->get();
        $pending_orders = \App\Order::where("status", 2)->get();
        $approved_orders = \App\Order::where("status", 3)->get();
        $sent_orders = \App\Order::where("status", 4)->get();

        return view("orders.order-list")
                        ->with("orders", $orders)
                        ->with("new_orders", $new_orders)
                        ->with("pending_orders", $pending_orders)
                        ->with("approved_orders", $approved_orders)
                        ->with("sent_orders", $sent_orders);
    }

    public function substractOrdered($pid, $qty) {

        $product = \App\Product::find(intval($pid));
        $product->quantity = intval($product->quantity) - intval($qty);
        if($product->status < 20)
            $product->status = 3;
        $product->save();
    }

    public function createOrder(Request $request) {

        $form_data = $request->formData;

        $order = new \App\Order();
        $order->unique_id = mt_rand(100000, 999999);
        $order->description = $form_data["description"];
        $order->shipment_types = $form_data["shipment"];
        $order->payment_types = $form_data["payment"];
        $order->address = $form_data["address"];
        $order->client = $form_data["client"];
        $order->status = $form_data["status"];
        $order->created_by = Auth::user()->username;
        $order->save();

        $created_order_id = $order->id;
        $orderList = $request->order_list;

        for ($var = 0; $var < count($orderList); $var++) {
            $orderProduct = new \App\OrderProduct();
            $orderProduct->order_id = $created_order_id;
            $orderProduct->product_id = intval($orderList[$var]["product_id"]);
            $orderProduct->price = floatval($orderList[$var]["price"]);
            $orderProduct->quantity = intval($orderList[$var]["quantity"]);
            $orderProduct->total = intval($orderList[$var]["quantity"]) * floatval($orderList[$var]["price"]);
            $orderProduct->save();
            $this->substractOrdered(
                    intval($orderList[$var]["product_id"]), intval($orderList[$var]["quantity"])
                ); // Substracting the ordered quantities from stock
        }

        event(new ProductNotification());
    }

    public function outOfStockSystemMessage($product) {

        if ($product->quantity <= 0) {
            $event = new \App\Event();
            $event->title = "Out Of Stock";
            $event->overview = "This is a system message.";
            $event->content = "<b>".$product->name . "</b> is out of stock. Please make sure you have all products in stock.";
            $event->status = "System Message";
            $event->author = "System";
            $event->save();
        }
    }

    public function editOrder(Request $request, $id) {

        $order = \App\Order::find($id);
        $order->name = $request->input("order-name");
        $order->description = $request->input("order-description");
        $order->end_price = $request->input("order-endprice");
        $order->b2b_price = $request->input("order-b2bprice");
        $order->vendor_price = $request->input("order-vendor-price");
        $order->vendor = $request->input("order-vendor");
        $order->status = $request->input("order-status");
        $order->completeness = $request->input("order-completeness");
        $order->lot = $request->input("order-lot");
        $order->delivery_date = $request->input("order-delivery-date");
        $order->expiry_date = $request->input("order-expiry-date");
        $order->created_by = Auth::user()->name;

        event(new App\Events\ProductNotification());

        if ($order->save()) {
            return redirect("orders/list");
        }
    }

    public function deleteOrder($id) {

        $order = \App\Order::find($id);
        $order->delete();
        return redirect("orders/list");
    }

    public function getSingleOrder($id) {

        $order = \App\Order::find($id);
        $ordered_products = [];
        $total = [];

        for ($i = 0; $i < count($order->products()); $i++) {
            $ordered_products[$i] = [
                "quantity" => $order->products()[$i]->quantity,
                "name" => \App\Product::find($order->products()[$i]->product_id)->name,
                "description" => \App\Product::find($order->products()[$i]->product_id)->description,
                "unit_price" => $order->products()[$i]->price,
                "sub_total" => floatval($order->products()[$i]->price) * intval($order->products()[$i]->quantity)
            ];
            $total[$i] = floatval($order->products()[$i]->price) * intval($order->products()[$i]->quantity);
        }

        $total = array_sum($total);

        return view("orders.order")
                        ->with("order", $order)
                        ->with("ordered_products", $ordered_products)
                        ->with("total", $total);
    }

    public function getDashboard() {

        return view('orders.dashboard');
    }
}
