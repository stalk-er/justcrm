<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{

    public function edit()
    {
        $settings = \App\Setting::all();
        $s = [];

        foreach ($settings as $key => $value) {
            $s[$value->key] = $value->value;
        }

        return view("settings.settings")->with("settings", $s);
    }

    public function update(Request $request)
    {
        $allSettings = $request->all();
        unset($allSettings["_token"]);

        if(!empty($allSettings)) {
            $setting = null;
            foreach($allSettings as $key => $value) {

                $setting = \App\Setting::where("key", $key);
                $data = [
                    "key"   =>  $key,
                    "value" =>  $value
                ];
                if(!$setting->exists()) {
                    $setting = new \App\Setting();
                    $setting->create($data);
                }
                else {
                    $setting->update($data);
                }
            }
        }

        return redirect()->back();
    }
}
