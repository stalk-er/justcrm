<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Input;
use File;

class QuoteTemplateController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view("quote-templates.quote-templates-list")->with("templates", \App\QuoteTemplate::paginate(10));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view("quote-templates.quote-template-actions");
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
        );

        $validator = Validator::make($request->input(), $rules);
        if ($validator->fails()) {
            return redirect()
            ->back()
            ->withErrors($validator)
            ->withInput();
        }

        $quote = new \App\QuoteTemplate();
        if($quote->create($request->input()))
        return redirect("quote-templates");
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        return view("quote-templates.quote-template-actions")->with("template", \App\QuoteTemplate::find($id));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $quoteTemplate = \App\QuoteTemplate::find($id);
        $dataToValidate = $request->input();


        $rules = [
            'name' => 'required',
            //'extension' => 'mimes:xlsx'
        ];
        $validator = Validator::make($dataToValidate, $rules);
        if ($validator->fails()) {
            return redirect()
            ->back()
            ->withErrors($validator)
            ->withInput();
        }
        // Validation passed

        $name = $dataToValidate["name"];
        unset($dataToValidate["_method"]);
        unset($dataToValidate["_token"]);


        if($quoteTemplate->update([
            "name"  => $name,
            "settings"  => json_encode($dataToValidate)
        ]))
        return redirect("quote-templates");
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        if(\App\QuoteTemplate::find($id)->delete())
        return redirect("quote-templates");
    }
}
