<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
    Laravel sortable tables
    https://github.com/larkinwhitaker/laravel-table-view
*/


Route::get("login", "LoginController@getLogin");
Route::get('logout', 'LoginController@getLogout');

// Login/Register
Route::post("login", "LoginController@postLogin");
Route::post("register", "RegisterController@postRegister");

// Confirm Email Link
Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'RegisterController@confirm'
]);

Route::group(['middleware' => 'auth'], function () {

    // Dashboard
    Route::get('dashboard', function () { return view('dashboard'); });
    Route::post('avatar/upload', 'DashboardController@uploadAvatar');

    // Products
    Route::get("products/list", "ProductController@getProducts");
    Route::get("products/new", function() {
        return view("products.product-actions")
        ->with("vendors", App\Vendor::all())
        ->with("product_statuses", \App\ProductStatus::all());
    });
    Route::post("products/new", "ProductController@createProduct");
    Route::get("products/store", "ProductController@storeExistingProduct");
    Route::get("products/{id}/edit", function($id) {
        return view("products.product-actions")
        ->with("vendors", App\Vendor::all())
        ->with("product", \App\Product::find($id))
        ->with("product_statuses", \App\ProductStatus::all());
    });
    Route::get("products/{barcode}/get", "ProductController@getSingleProductByBarcode");
    Route::post("products/{barcode}/store", "ProductController@storeSingleProductByBarcode");
    Route::post("products/{id}/preview", "ProductController@getSingleProduct");
    Route::post("products/{id}/edit", "ProductController@editProduct");
    Route::post("products/{id}/delete", "ProductController@deleteProduct");
    Route::get("products/{id}/details", "ProductController@getProductDetails");
    Route::get("/products/json", "ProductController@getProductsAsJson");
    Route::get("/products/byTerm", "ProductController@getProductByTerm");

    // Orders
    Route::get("orders/dashboard", "OrderController@dashboard");
    Route::get("orders/doughnut", "OrderController@ordersDoughnut");
    Route::resource("orders", "OrderController");

    // Vendors
    Route::resource("vendors", "VendorController");

    // Services
    Route::resource("services", "ServiceController");

    // Quotes
    Route::resource("quotes", "QuoteController");

    // Quotes
    Route::resource("quote-templates", "QuoteTemplateController");

    // Shipment Types
    Route::resource("shipment-types", "ShipmentTypeController");

    // Payment Types
    Route::resource("payment-types", "PaymentTypeController");

    // User Settings
    Route::resource("user-profile", "UserProfileController");

    // System Settings
    Route::get("settings", "SystemSettingsController@edit");

    // Calendar
    Route::resource("calendar", "CalendarController");

    // Event calendar
    Route::get("event-calendar", "EventController@getCalendar");
    Route::get("calendar/events/get", "EventController@fetchCalendarEvents");
    Route::post("calendar/event/save", "EventController@saveCalendarEvent");
    Route::post("calendar/event/update", "EventController@updateCalendarEvent");
    Route::post("calendar/event/delete", "EventController@deleteCalendarEvent");

    // Events
    Route::get("events", "EventController@getEvents");
    Route::get("events/{id}/details", "EventController@getEventDetails");
    Route::post("events/comment/save", "EventController@saveComment");
    Route::post("events/fetch-comments", "EventController@fetchComments");
    Route::post("/mark-read", "NotificationEventController@markRead");


    // Application settings
    Route::get("/settings", "SettingController@edit");
    Route::post("/settings/update", "SettingController@update");
});
