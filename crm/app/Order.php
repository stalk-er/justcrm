<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    public $incrementing = true;
    protected $table = "orders";
    protected $fillable = [

        "unique_id",
        "description",
        "shipment_types",
        "payment_types",
        "address",
        "client",
        "status",
        "created_by"

    ];


    public function products() {

        return $this->hasMany('App\OrderProduct');
    }


    public function status() {

       return $this->BelongsTo('App\OrderStatus', 'status', 'id');
    }

    public function invoice(){

        return $this->hasOne('\App\Invoice', 'id');
    }

    public function shipment() {

        return $this->hasOne('\App\ShipmentType', 'id');
    }
    public function payment() {

        return $this->hasOne('\App\PaymentType', 'id');
    }

}
