<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteTemplate extends Model
{
    protected $table = "quote_templates";
    protected $fillable = [
        "name",
        "settings"
    ];
}
