<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model {

    protected $table = 'order_product';
    protected $fillable = [

        "order_id",
        "product_id"

    ];

    public function getOriginalModel()
    {
        return \App\Product::find($this->product_id);
    }
}
