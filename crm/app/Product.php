<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    protected $table = 'products';
    protected $fillable = [
        "name",
        "description",
        "end_price",
        "b2b_price",
        "vendor_price",
        "vendor", "vendor",
        "status",
        "quantity",
        "completeness",
        "lot",
        "delivery_date",
        "expiry_date"
    ];

    public function current_status() {
        return $this->hasOne("App\ProductStatus", "id", "status");
    }

    public function vendor() {
        return $this->belongsTo("App\Vendor");
    }
}
