<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'vendors';
    protected $fillable = [

        "name",
        "description",
        "city",
        "country",
        "address",
        "zip_code",
        "email",
        "telephone",
        "fax",
        "website",
        "location_lon",
        "location_lat",
        "avatar_image",
    ];


    public function products() {

        return $this->hasMany("\App\Product", "vendor")->get();
    }

}
