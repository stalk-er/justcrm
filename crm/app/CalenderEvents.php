<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalenderEvents extends Model
{

    protected $table = "calender_events";
    protected $fillable = [
        "title",
        "description",
        "start",
        "end",
        "allDay"
    ];
}
