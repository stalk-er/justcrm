<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $table = 'quotes';
    protected $fillable = [
        "name",
        "unique_id",
        "content",
        "client",
        "template",
        "shipment_type",
        "quote_items",
        "created_by"
    ];

    public function shipmentType()
    {
        return $this->hasOne("\App\ShipmentType", "id", "shipment_type");
    }

    public function qTemplate()
    {
        return $this->hasOne("\App\QuoteTemplate", "id", "template");
    }
}
