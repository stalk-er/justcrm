function disappearSlow() {
    setTimeout(function () {
        $(".alert").fadeOut();
    }, 1000);
}

function push_notification(title, msg, type) {
    new PNotify({
        title: title,
        text: msg,
        type: type
    });
}

function log(data) {
    console.log(data);
    return;
}

function DecimalNumberWrapper() {

    this.format = function (num, precision) {
        return parseFloat((Math.round(num * 100) / 100).toFixed(precision)); // 123.75
    }
}


// Put confirmation message before each delete button click
$(document).on("click", ".delete", function (e) {
    var c = confirm("Are you sure you want to delete this item?");
    if(c) return true;
    else return false;
});

function TableModifier(tableId, messages) {

    var me = this;

    this.noItems = $("#noItems");

    this.decimalFormater = new DecimalNumberWrapper();

    this.table = $(tableId);

    this.warning = messages.warning;

    this.error = messages.error;

    this.success = messages.success;

    this.addRowToBody = function (tableRow) {

        if(this.table.find("[data-item-id]").length == 0) {
            this.noItems.hide();
        }

        if(this.table.find("[data-item-id='" + tableRow.data("item-id") + "']").length > 0) {
            push_notification(this.warning.title, this.warning.text, "warning");
            return false;
        }

        $(this.table).find("tbody").append(tableRow);
        push_notification(this.success.title, this.success.text, "success");
    };

    this.getItemsTotalPrice = function () {

        if(me.table.find(".subtotal").length > 0) {
            var total = me.table.find(".subtotal").map(function (index, element) {
                return $(element).text()
            }).get().reduce(function(previousValue, currentValue) {
                return me.decimalFormater.format(
                    Number(currentValue) + Number(previousValue),
                    2);
                });
                return me.decimalFormater.format(Number(total), 2);
            }
            return 0;
    }

    this.updateTotalPriceWithTaxation = function () {
        $("#totalWithTax").text(me.calculateTotalPriceWithTaxation());
    };

    this.updateTaxation = function () {
        $("#tax").text(me.getTaxation(me.getItemsTotalPrice())); // 20/100 * total
    };

    this.calculateTotalPriceWithTaxation = function () {
        return me.decimalFormater.format(
            me.getTaxation(
                me.getItemsTotalPrice()
            ) + me.getItemsTotalPrice()
            + me.shippingPrice()
            , 2);
    };

    this.shippingPrice = function () {
        return Number($("#shipping").text());
    };

    this.updateShippingPrice = function (val) {
        $("#shipping").text(val);
    };

    this.calibratePrices = function () {

        var IntSubTotal = $(this).closest("tr").find(".unitPrice").val() * $(this).closest("tr").find(".quantity").val();
        $(this).closest("tr").find(".subtotal").text(me.decimalFormater.format(IntSubTotal, 2));

        me.updateTaxation();
        $("#total, #subTotal").text(me.getItemsTotalPrice());
        me.updateTotalPriceWithTaxation();
    };

    this.getTaxation = function (total) {
        if (typeof total == "undefined") {
            console.error("No argument suplied for this function");
        }
        var taxValue = Number( $("#tax").data("tax") );
        return me.decimalFormater.format(taxValue / 100 * total, 2);
    };

    this.incrementProductCount = function () {
        $("#itemsCount").val(
            Number($("#itemsCount").val()) + 1
        );
    }

    this.generateTableRow = function (data) {

        var tr = $("<tr>");
        var availableProducts = Number( $("#itemsCount").val() );

        tr.attr("data-item-id", data.id);
        tr.append($('<input type="hidden" name="items['+availableProducts+'][id]" />').val(data.id))
        tr.append($('<input type="hidden" name="items['+availableProducts+'][name]" />').val(data.name))

        var name = $('<td>').text(data.name); // name
        tr.append(name);

        var quantity = $('<td>').append(
            $('<input name="items['+availableProducts+'][quantity]" type="number" min="0" max="' + data.quantity + '" class="quantity form-control" />')
            .val(0)
            .keyup(me.calibratePrices)
            .on("change", function () { // on value change
                if( Number($(this).val()) > Number($(this).attr("max")) )
                $(this).val($(this).attr("max"))
            })
        ); tr.append(quantity);

        var price = $('<td>').append(
            $('<input step="any" name="items['+availableProducts+'][unit_price]" class="unitPrice form-control" />')
            .val(data.end_price).keyup(me.calibratePrices)
        ); tr.append(price);

        var subTotal = $("<td class='subtotal'>").text(0);
        tr.append(subTotal);
        me.incrementProductCount();
        return tr;
    }

    this.constructor = function () {

        var cp;

        this.table.find(".quantity").keyup(function () {
            cp = me.calibratePrices.bind(this);
            cp();
        });

        this.table.find(".unitPrice").keyup(function () {
            cp = me.calibratePrices.bind(this);
            cp();
        });

        this.table.find("#changeShipmentType").change(function () {
            cp = me.calibratePrices.bind(this);
            cp();
        });

        this.calculateTotalPriceWithTaxation()
    }

    this.constructor();
};
