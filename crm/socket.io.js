//require modules
var express = require('express');
var app     = express();
var http    = require('http').Server(app);
var socket  = require('socket.io')(http);
var Redis   = require('ioredis');
var redis   = new Redis();

//configuration
var port = 3000;

//subscribe to test-channel on redis
redis.subscribe('ProductNotificationChanel', function(error, count) {
    console.log('COUNT', count)
});

//message event listener
redis.on('message', function(channel, message) {

	console.log('Message received: ' + message);
    console.log(message)
	message = JSON.parse(message);
	socket.emit(channel + ':' + message.event, message.data);
});

//server listener
http.listen(port, function(){
	console.log('Server running on port: ' + port);
});
