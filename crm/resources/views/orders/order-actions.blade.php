@extends("master")
@section("content")
<!-- Drag and Drop -->
<link rel="stylesheet" href="{!! URL::asset('assets/css/dragndrop/jquery-ui.min.css') !!}">
<link rel="stylesheet" href="{!! URL::asset('assets/css/dragndrop/jquery-ui.structure.min.css') !!}">
<link rel="stylesheet" href="{!! URL::asset('assets/css/dragndrop/jquery-ui.theme.min.css') !!}">
<script type="text/javascript" src="{!! URL::asset('assets/js/dragndrop/jquery-ui.min.js') !!}"></script>

<style type="text/css">
    .panel-heading { z-index: 999 !important; text-decoration: none !important; }
    .panel-heading .panel-title { display: inline-block; }
    .panel-heading .panel-item-controls { float: none; width: 100%; display: block; }
    .panel-heading .panel-item-controls label { margin-right: 10px; display: inline-block; }
    .panel-heading .panel-item-controls select.form-control { width: 50%; display: inline-block; }
    label.price-label { margin-right: 10px; }

    .order-list { padding: 0 5px 6px; position: relative; width: 100%; }
    .accordion .panel { margin-top: 10px; }
    .ui-state-active { background-color: #f2f2f2; border: 1px solid #bfbfbf; }
    .ui-state-active * { color: inherit !important; }
    .details-show { margin-left: 10px; display: inline-block; }
    .modal .custom-price { display: none; }
    input[type="number"].form-control { line-height: normal; }
</style>

<?php $itemsTotal = 0; ?>
<div class="row">
    @if(isset($order))
    {{ Form::model($order, ['route' => ['orders.update', $order->id], 'method' => 'PUT', 'class' => 'form-horizontal form-label-left']) }}
    @else
    {{ Form::open(['url' => 'orders', 'class' => 'form-horizontal form-label-left']) }}
    @endif
    {{
        Form::hidden('itemsCount', (isset($products)) ? count($products) : 0, [ "id" => "itemsCount" ])
    }}
    <!-- Order information -->
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-align-left"></i> Order information </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a></li>
                            <li><a href="#">Settings 2</a></li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Order description</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::textarea('description', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Enter description',
                                    'rows' => '3'
                                    ])
                                }}
                                @if ($errors->has('description'))
                                <span class="help-block red">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Client</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('client', null, [
                                'class' => 'form-control',
                                    'placeholder' => 'Client name',
                                    'style' => 'width: 80%; display: inline-block; vertical-align: middle;'
                                    ])
                                }}
                                @if ($errors->has('client'))
                                <span class="help-block red">
                                    <strong>{{ $errors->first('client') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Order status</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select class="order-statuses form-control" tabindex="-1" name="status">
                                    @foreach($order_statuses as $status)
                                    <option
                                        @if(isset($order))
                                            @if($status->id == $order->status) selected @endif
                                        @endif
                                    value="{!! $status->id !!}">{!! $status->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::textarea('address', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Enter address',
                                    'rows' => '3'
                                    ])
                                }}
                                @if ($errors->has('address'))
                                <span class="help-block red">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Payment</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select name="payment_types" class="payment-types form-control" style="display: inline-block; width: 50%;"
                                    class="form-control" id="changePaymentType">
                                    @foreach($payment_types as $payment)
                                    <option
                                        @if(isset($order))
                                            @if($payment->id == $order->payment_types) selected @endif
                                        @endif
                                        value="{{ $payment->id }}">
                                        {{ $payment->name }}
                                    </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('payment_types'))
                                <span class="help-block red">
                                    <strong>{{ $errors->first('payment_types') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class=""> &nbsp; </div>
                    <div class=""> &nbsp; </div>
                    <div class=""> &nbsp; </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Choose a product</label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <select disabled="disabled" id="chooseProduct" class="products form-control" tabindex="-1">
                                    <option value=""><option>
                                    @if(count($stock_products) > 0)
                                        @foreach($stock_products as $product)
                                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <button name="addProduct" id="addProduct" disabled="disabled" class="btn btn-success btn-md">Add Product</button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <!-- /Order information -->

    <div class="col-md-6 col-sm-12 col-xs-12" id="order-list">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-align-left"></i> Ordered products </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a></li>
                            <li><a href="#">Settings 2</a></li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="accordion order-list" role="tablist" aria-multiselectable="true">
                    <table class="table table-striped" id="orderProductsList">
                        <thead>
                            <tr>
                                <th>Product name</th>
                                <th>Quantity</th>
                                <th>Unit price</th>
                                <th>Sub total</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php $total = 0; ?>
                            @if(isset($products))
                                @if(count($products) > 0)
                                    <?php $p = 0; ?>
                                    @foreach($products as $product)
                                    <tr data-item-id="{{ $product->getOriginalModel()->id }}">
                                        <input type="hidden" name="items[{{ $p }}][order_product_id]" value="{{ $product->id }}" />
                                        <td>{{ $product->getOriginalModel()->name }}</td>
                                        <td><input class="quantity form-control" type="number" name="items[{{ $p }}][quantity]" value="{{ $product->quantity }}" min="0" max="{{ $product->getOriginalModel()->quantity }}" /></td>
                                        <td><input step="any" class="unitPrice form-control" type="number" name="items[{{ $p }}][unit_price]" value="{{ $product->price }}" min="0" /></td>
                                        <td class="subtotal">{{ $product->total }}</td>
                                    </tr>
                                    <?php $p++; $total+=$product->total; ?>
                                    @endforeach
                                @endif
                            @else
                                <tr id="noItems">
                                    <td colspan="4">
                                        Chosen products will appear here
                                    </td>
                                </tr>
                            @endif
                        </tbody>

                        <tfoot>
                            <tr>
                                <td colspan="4" style="text-align:right;">Total &nbsp;
                                    <span id="total">
                                        {{ $total }}
                                    </span>
                                </td>
                                <td><strong></strong></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-align-left"></i> Total </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a></li>
                            <li><a href="#">Settings 2</a></li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <p class="lead">Total with taxes</p>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th style="width:30%">Subtotal:</th>
                                <td></td>
                                <td id="subTotal">{{ $total }}</td>
                            </tr>
                            <tr>
                                <th width="30%">Tax (20%)</th>
                                <td></td>
                                <?php
                                $taxation = 20 / 100 * $total;
                                ?>
                                <td data-tax="20" id="tax">{{ $taxation }}</td>
                            </tr>
                            <tr>
                                <th width="30%">Shipping:</th>
                                <td>
                                        @if(count($shipment_types) > 0)
                                        <?php $shipmentPrice = 0; ?>
                                        <select name="shipment_types" style="display: inline-block; width: 50%;"
                                        class="form-control" id="changeShipmentType">
                                        @foreach($shipment_types as $shipmentType)
                                        <option
                                        @if(isset($order))
                                        @if($shipmentType->id == $order->shipment_types)
                                        selected
                                        <?php $shipmentPrice =  $shipmentType->price; ?>
                                        @endif
                                        @endif
                                        value="{{ $shipmentType->id }}"
                                        data-shipment-price="{{ $shipmentType->price }}">{{ $shipmentType->name }}</option>
                                        @endforeach
                                    </select>
                                    @else
                                    It seems no shimpment types exist
                                    @endif
                                </td>
                                <td id="shipping">{{ $shipmentPrice }}</td>
                            </tr>
                            <tr>
                                <th width="30%">Total:</th>
                                <td></td>
                                <td id="totalWithTax">{{ $shipmentPrice + $total + $taxation }}</td>
                            </tr>
                            <tr>
                                <td colspan="4"><input type="submit" class="btn btn-success btn-lg" value="Save order" /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}

</div>

<script type="text/javascript">
$(document).ready(function ($) {

    /* Allow choosing products only if the order information is populated */
    $("#order-information input[type='text'], #order-information input[type='number'], textarea").on("keyup load", function (e) {

        $("#order-information input[type='text'], #order-information input[type='number'], textarea").each(function () {
            if( $(this).val() != "" || $(this).text() != "" ) {
                $("#chooseProduct").attr("disabled", false);
                $("#addProduct").attr("disabled", false);
            } else {
                $("#chooseProduct").attr("disabled", true);
                $("#addProduct").attr("disabled", true);
                return false;
            }
        });
    });

    /* Choose a product */
    $('#chooseProduct').select2({
        placeholder: "Choose a product",
        //minimumInputLength: 3,
        multiple: false,
        quietMillis: 100,
        allowClear: true,
        ajax: {
            type: 'GET',
            url: window.location.origin + "/products/json",
            dataType: 'json',
            data: function (json) {
                return {
                    term: json.term,
                    delay: 0.3
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.name, text: obj.name };
                    })
                };
            },
        }
    });

    var ProductsTableModifier = new TableModifier("#orderProductsList", {
        warning: {
            title: "The product already exists.",
            text: "The product already exists in the order list. Please choose another one.",
        },
        error: {
            title: "Product not found.",
            text: "The desired product was not found in the database.",
        },
        success: {
            title: "Product successfully added.",
            text: "The product was successfully added to the order list.",
        }
    });

    $("#changeShipmentType").on("change", function () {
        console.log(
            this
        )
        var shipmentPrice = Number($(this).find(":selected").data("shipment-price"));
        $("#shipping").text(shipmentPrice);
        ProductsTableModifier.updateTotalPriceWithTaxation();
    });

    /* Add new product*/
    $("#addProduct").on("click", function (e) {
        e.preventDefault();
        $.ajax({
            url: window.location.origin + "/products/byTerm",
            type: "GET",
            data: {
                term: $("#chooseProduct").val()
            },
            success: function (product) {
                ProductsTableModifier.addRowToBody(
                    ProductsTableModifier.generateTableRow(
                        product
                    )
                );
            },
            error: function () {
                push_notification(ProductsTableModifier.error.title, ProductsTableModifier.error.text, "error");
            }
        });
    })
});
</script>
@stop
