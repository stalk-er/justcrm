@extends('master')
@section('content')

<div class="">
    <div class="page-title">
        <div class="title_left">&nbsp;</div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>quotes</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a></li>
                                <li><a href="#">Settings 2</a></li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped quotes">
                        <thead>
                            <tr>
                                <th style="width: 1%">#</th>
                                <th style="width: 40%">UID</th>
                                <th>Client</th>
                                <th style="width: 20%">#Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($orders) > 0)
                            @foreach($orders as $order)
                            <tr>
                                <td>#</td>
                                <td>
                                    <a>{!! $order->unique_id !!}</a>
                                    <br />
                                    <small>{!! $order->created_at !!}</small>
                                </td>
                                <td>{{ $order->client }}</td>
                                <td>
                                    <a href="/orders/{!! $order->id !!}" data-id="{!! $order->id !!}" class="btn btn-primary btn-xs preview"><i class="fa fa-folder"></i> View </a>
                                    <a href="/orders/{!! $order->id !!}/edit" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                    {{ Form::open(array('url' => 'orders/' . $order->id, 'style' => 'display: inline-block; vertical-align: middle;')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs delete')) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="3">There's no orders created yet</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    {{ $orders->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
