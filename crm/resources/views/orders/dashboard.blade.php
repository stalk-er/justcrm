@extends('master')

@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel tile overflow_hidden">
            <div class="x_title">
                <h2>Device Usage</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a></li>
                            <li><a href="#">Settings 2</a></li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table class="" style="width:100%">
                    <tr>
                        <th style="width:37%;">
                            <p>Orders</p>
                        </th>
                        <th>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <p class="">Status</p>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <canvas id="canvas1" height="250" width="250" style="margin: 15px 10px 10px 0"></canvas>
                        </td>
                        <td>
                            <table class="tile_info">
                                @foreach($statuses as $status)
                                <tr>
                                    <td>
                                        <p><i class="fa fa-square" style="color: {{ $status->color }};"></i>{{ $status->name }} </p>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">

    jQuery(document).ready(function ($) {

        var doughnutData = [];

        $.ajax({
            url: window.location.origin + "/orders/doughnut",
            method: "GET",
            data: {
                _token: $("#token").attr("content")
            },
            success: function (data) {

                var status;
                var ordersByStatus = [];
                var byStatus = [];

                if(data.length > 0)
                    status = data[0].status.id;

                for(var x in data) {
                    if(data[x].status.id == status)
                    byStatus.push(data[x]);
                    else {
                        ordersByStatus.push(byStatus);
                        byStatus = [];
                        status = data[x].status.id;
                    }
                }

                for(var i in ordersByStatus) {
                    doughnutData.push({
                        label: ordersByStatus[i][0].status.name,
                        color: ordersByStatus[i][0].status.color,
                        value: ordersByStatus[i].length
                    });
                }

                new Chart(
                    document.getElementById("canvas1").getContext("2d")
                ).Doughnut(doughnutData)
            },
            error: function (error) {
                push_notification("Orders couldn't load", "The orders couldn't load. Please, refresh the page", "error");
            }
        });
    });
    </script>
    @stop
