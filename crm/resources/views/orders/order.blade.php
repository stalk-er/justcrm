@extends('master')

@section('content')
<style type="text/css">
    @media print {
        div.nav_menu { display: none; }
        button.submit, button.generate, button.print { display: none; }
        ul.nav.navbar-right.panel_toolbox { display: none; }
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Invoice Design <small>Sample user invoice design</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a></li>
                            <li><a href="#">Settings 2</a></li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <section class="content invoice">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-xs-12 invoice-header">
                            <h1>
                                <i class="fa fa-globe"></i> Invoice.
                                <small class="pull-right">Date: 16/08/2016</small>
                            </h1>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            From
                            <address>
                                My company headquaters address
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            To
                            <address>
                                <strong>{!! $order->client !!}</strong>
                                <br>{!! $order->address !!}
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            <b>Invoice #{!! $order->unique_id !!}</b>
                            <br>
                            <br>
                            <b>Order ID:</b> {!! $order->id !!}
                            <br>
                            <b>Payment Due:</b> 2/22/2014
                            <br>
                            <b>Account:</b> 968-34567
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-xs-12 table">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Quantity</th>
                                        <th>Product</th>
                                        <th>Description</th>
                                        <th>Unit price</th>
                                        <th>Sub total</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($ordered_products as $product)
                                    <tr>
                                        <td>{!! $product["quantity"] !!}</td>
                                        <td>{!! $product["name"] !!}</td>
                                        <td>{!! $product["description"] !!}</td>
                                        <td>{!! $product["unit_price"] !!}</td>
                                        <td>{!! $product["sub_total"] !!}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4" style="text-align:right;">Total</td>
                                        <td><strong>{!! $total !!}</strong></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-6">
                            <p class="lead">Payment Methods:</p>
                            <img src="{!! URL::asset('assets/images/visa.png') !!}" alt="Visa">
                            <img src="{!! URL::asset('assets/images/mastercard.png') !!}" alt="Mastercard">
                            <img src="{!! URL::asset('assets/images/american-express.png') !!}" alt="American Express">
                            <img src="{!! URL::asset('assets/images/paypal2.png') !!}" alt="Paypal">
                            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">Some payment information</p>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-6">
                            <p class="lead">Amount Due 2/22/2014</p>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th style="width:50%">Subtotal:</th>
                                            <td>$250.30</td>
                                        </tr>
                                        <tr>
                                            <th>Tax (9.3%)</th>
                                            <td>$10.34</td>
                                        </tr>
                                        <tr>
                                            <th>Shipping:</th>
                                            <td>$5.80</td>
                                        </tr>
                                        <tr>
                                            <th>Total:</th>
                                            <td>$265.24</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-xs-12">
                            <button class="btn btn-default print" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                            <button class="btn btn-success pull-right submit"><i class="fa fa-credit-card"></i> Submit Payment</button>
                            <button class="btn btn-primary pull-right generate" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

@stop
