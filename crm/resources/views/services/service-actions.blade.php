@extends("master")

@section("content")
<div class="row">
    @if(isset($service))
    {{ Form::model($service, ['route' => ['services.update', $service->id], 'method' => 'PUT', 'class' => 'form-horizontal form-label-left']) }}
    @else
    {{ Form::open(['url' => 'services', 'class' => 'form-horizontal form-label-left']) }}
    @endif
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Create/Edit service</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a></li>
                            <li><a href="#">Settings 2</a></li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="col-md-6">
                    <div class="x_title">
                        <h2>service base information <small> name, service, prices</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <br>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter name']) }}
                            @if ($errors->has('name'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Enter description']) }}
                            @if ($errors->has('description'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Price</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::number('price', null, ['class' => 'form-control', 'placeholder' => 'Price']) }}
                            @if ($errors->has('price'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('price') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Execution time</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::text('execution_time', null, ['class' => 'form-control', 'placeholder' => 'Execution time']) }}
                            @if ($errors->has('execution_time'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('execution_time') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                        <a href="{{ URL::previous() }}" class="btn btn-primary">Cancel</a>
                        <input type="submit" value="{!! (isset($service)) ? 'Save' : 'Save' !!}" class="btn btn-success" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>

<!-- Date picker -->
<script type="text/javascript">
$(document).ready(function () {
    $('#single_cal1').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        locale: {
            format: "DD/MM/YYYY"
        },
    }, function (start, end, label) {

        $("#delivery_date_value").val(start.toISOString());
    });
    $('#single_cal2').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        locale: {
            format: "DD/MM/YYYY"
        },
    }, function (start, end, label) {

        $("#expiry_date_value").val(start.toISOString());
    });
});
</script>
<!-- /Date picker -->
@stop
