@extends("master")

@section("content")
<div class="row">
    @if(isset($quote))
    {{ Form::model($quote, ['route' => ['quotes.update', $quote->id], 'method' => 'PUT', 'class' => 'form-horizontal form-label-left']) }}
    @else
    {{ Form::open(['url' => 'quotes', 'class' => 'form-horizontal form-label-left']) }}
    @endif
    {{ Form::hidden('itemsCount', (isset($quoteItems)) ? count($quoteItems) : 0, [
    "id" => "itemsCount"
    ]) }}
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Quote</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Preview</a></li>
                            <li><a href="#" onclick="window.print(); return false;">Print</a></li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <section class="content invoice">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-xs-12 invoice-header">
                            <h1>
                                <i class="fa fa-globe"></i>
                                {{ Form::text('name', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Enter quote name',
                                    'style' => 'width: 80%; display: inline-block; vertical-align: middle;'
                                    ])
                                }}
                            @if(isset($quote))<small class="pull-right">Date: {{ $quote->updated_at }}</small>@endif
                        </h1>
                        @if ($errors->has('name'))
                        <span class="help-block red">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <!-- /.col -->
                </div>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">From <address> <strong>My company</strong> </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        To
                        <address>
                            <strong>
                                {{ Form::text('client', null, ['class' => 'form-control', 'placeholder' => 'Enter client name']) }}
                            </strong>
                        </address>
                        @if ($errors->has('client'))
                        <span class="help-block red">
                            <strong>{{ $errors->first('client') }}</strong>
                        </span>
                        @endif
                    </div>
                    <!-- /.col -->

                    <br>
                    <br>
                    <br>

                    @if(isset($quote))
                    <div class="col-sm-4 invoice-col">
                        <strong>Quote ID #{!! $quote->unique_id !!}</strong>
                    </div>
                    @endif
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-xs-12 table">
                        <table class="table table-striped" id="quoteProductsList">
                            <thead>
                                <tr>
                                    <th>Product name</th>
                                    <th>Quantity</th>
                                    <th>Unit price</th>
                                    <th>Sub total</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $itemsTotal = 0; $itemCounter = 0; ?>
                                @if(isset($quoteItems))

                                @foreach($quoteItems as $item)
                                <tr>
                                    <input type="hidden" name="items[{{ $itemCounter }}][id]" value="{{ $item->id }}" />
                                    <input type="hidden" name="items[{{ $itemCounter }}][name]" value="{{ $item->name }}" />
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <input name="items[{{ $itemCounter }}][quantity]" type="number" min="0" max="" value="{{ $item->quantity }}" class="quantity form-control" />
                                    </td>
                                    <td>
                                        <input name="items[{{ $itemCounter }}][unit_price]" value="{{ $item->unit_price }}" class="unitPrice form-control" />
                                    </td>
                                    <td class="subtotal">{{ $item->unit_price * $item->quantity }}</td>
                                    <?php $itemsTotal +=  $item->unit_price * $item->quantity; ?>
                                </tr>
                                <?php $itemCounter++; ?>
                                @endforeach
                                @else
                                <tr id="noItems">
                                    <td colspan="4">No items</td>
                                </tr>
                                @endif
                            </tbody>

                            <tfoot>
                                <tr>
                                    <td colspan="4" style="text-align:right;">Total &nbsp;
                                        <span id="total">
                                            @if(isset($quote))
                                            {{ $itemsTotal }}
                                            @else
                                            {{ 0 }}
                                            @endif
                                        </span>
                                    </td>
                                    <td><strong></strong></td>
                                </tr>
                            </tfoot>
                        </table>
                        <button class="btn btn-info" id="addQuoteProduct"><span style="padding-right: 10px;">Add Product</span> <i class="fa fa-plus"></i></button>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Quote template <span class="required">*</span></label>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    {{ Form::select('template', $quoteTemplates, null, ['class' => 'form-control' ]) }}
                                    @if ($errors->has('template'))
                                    <span class="help-block red">
                                        <strong>{{ $errors->first('template') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                            {{ Form::textarea('content', null, ['class' => 'form-control', 'placeholder' => 'Enter quote description']) }}
                            @if ($errors->has('content'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('content') }}</strong>
                            </span>
                            @endif
                        </p>
                        <p class="lead">Available Payment Methods:</p>
                        <img src="{!! URL::asset('assets/images/visa.png') !!}" alt="Visa" />
                        <img src="{!! URL::asset('assets/images/mastercard.png') !!}" alt="Mastercard" />
                        <img src="{!! URL::asset('assets/images/american-express.png') !!}" alt="American Express" />
                        <img src="{!! URL::asset('assets/images/paypal2.png') !!}" alt="Paypal" />
                        <br>
                        <br>
                    </div>

                    <!-- /.col -->
                    <div class="col-xs-6">
                        <p class="lead">Amount Due 2/22/2014</p>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th style="width:30%">Subtotal:</th>
                                        <td></td>
                                        <td id="subTotal">{{ $itemsTotal }}</td>
                                    </tr>
                                    <tr>
                                        <th width="30%">Tax (20%)</th>
                                        <td></td>
                                        <?php
                                        $taxation = 20 / 100 * $itemsTotal;
                                        ?>
                                        <td data-tax="20" id="tax">{{ $taxation }}</td>
                                    </tr>
                                    <tr>
                                        <th width="30%">Shipping:</th>
                                        <td>
                                            @if(count($shipmentTypes) > 0)
                                            <?php $shipmentPrice = 0; ?>
                                            <select name="shipment_type" style="display: inline-block; width: 50%;"
                                            class="form-control" id="changeShipmentType">
                                            @foreach($shipmentTypes as $shipmentType)
                                            <option
                                            @if(isset($quote))
                                            @if($shipmentType->id == $quote->shipment_type)
                                            selected
                                            <?php $shipmentPrice =  $shipmentType->price; ?>
                                            @endif
                                            @endif
                                            value="{{ $shipmentType->id }}"
                                            data-shipment-price="{{ $shipmentType->price }}">{{ $shipmentType->name }}</option>
                                            @endforeach
                                        </select>
                                        @else
                                        It seems no shimpment types exist
                                        @endif
                                    </td>
                                    <td id="shipping">{{ $shipmentPrice }}</td>
                                </tr>
                                <tr>
                                    <th width="30%">Total:</th>
                                    <td></td>
                                    <td id="totalWithTax">{{ $shipmentPrice + $itemsTotal + $taxation }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <input type="submit" class="btn btn-success btn-lg" value="Save" />
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
</div>
</div>
{{ Form::close() }}
</div>


<div id="addQuoteProductModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">New Calender Entry</h4>
            </div>
            <div class="modal-body">
                <div id="testmodal" style="padding: 5px 20px;">
                    <form id="antoform" class="form-horizontal calender" role="form">
                        <div class="form-group">
                            <label class="col-sm-12 control-label">Type product name:</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="pName" name="pName" style="margin-top: 15px;">
                                <div id="autocomplete-container" style="position: relative; float: left; width: 400px; margin: 10px;"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary antosubmit" id="findProduct">Find</button>
            </div>
        </div>
    </div>
</div>

<!-- Triggerer -->
<div hidden id="showProductsModal" data-toggle="modal" data-target="#addQuoteProductModal"></div>

<!-- Date picker -->
<script type="text/javascript">
$(document).ready(function () {
    $('#single_cal1').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        locale: {
            format: "DD/MM/YYYY"
        },
    }, function (start, end, label) {

        $("#delivery_date_value").val(start.toISOString());
    });
    $('#single_cal2').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        locale: {
            format: "DD/MM/YYYY"
        },
    }, function (start, end, label) {

        $("#expiry_date_value").val(start.toISOString());
    });
});
</script>
<!-- /Date picker -->



<!-- Autocomplete -->
<script type="text/javascript">
$(document).ready(function () {

    function highlightText (text, $node) {

        if (text == " ")
            return;

        var searchText = $.trim(text).toLowerCase(), currentNode = $node.get(0).firstChild, matchIndex, newTextNode, newSpanNode;
        while ((matchIndex = currentNode.data.toLowerCase().indexOf(searchText)) >= 0) {
            newTextNode = currentNode.splitText(matchIndex);
            currentNode = newTextNode.splitText(searchText.length);
            newSpanNode = document.createElement("span");
            newSpanNode.className = "highlight";
            currentNode.parentNode.insertBefore(newSpanNode, currentNode);
            newSpanNode.appendChild(newTextNode);
        }
    };

    var url = window.location.origin + "/products/json";
    var ProductsTableModifier = new TableModifier("#quoteProductsList", {
        warning: {
            title: "The product already exists.",
            text: "The product already exists in the quote list.",
        },
        error: {
            title: "Product not found.",
            text: "The chosen product was not found in the database.",
        },
        success: {
            title: "Product successfully added.",
            text: "The product was successfully added to the quote list.",
        }
    });

    var autocompleteArgs = {
        source: url,
        appendTo: "#autocomplete-container",
        select: function (ev, item) {}
    };

    function triggerModal() {
        $("#showProductsModal").click();
    };


    // Initialize autocomplete with custom appendTo:
    $('#pName').autocomplete(autocompleteArgs)
    .data("ui-autocomplete")._renderItem = function (ul, item) {

        var $a = $("<a></a>").css({
            "display" : "inline-block"
        }).text(item.label).css({ "width": "100%" });



        highlightText(this.term, $a);

        return $("<li class='product-item product-autocomplete-item'>").data("ui-autocomplete-item", item)
        .append(
            $a.prepend(function () {
                if(item.avatar)
                return $("<img>").attr("width", 50).attr("height", 50).attr("src", "/" + item.avatar);
                return "";
            })
            .append(function () {
                return $("<span>").text(" - Quantity: " + item.quantity + " units")
            })
        )
        .appendTo(ul);
    };

    $("#changeShipmentType").on("change", function () {
        console.log("Im here")
        var shipmentPrice = Number($(this).find(":selected").data("shipment-price"));
        $("#shipping").text(shipmentPrice);
        ProductsTableModifier.updateTotalPriceWithTaxation();
    });

    $("#addQuoteProduct").on("click", function (e) {
        e.preventDefault();

        // Show modal
        triggerModal();

    });

    $("#findProduct").on("click", function (e) {
        e.preventDefault();

        $.ajax({
            url: window.location.origin + "/products/byTerm",
            type: "get",
            data: {
                term: $("#pName").val()
            },
            success: function (quoteItem) {

                ProductsTableModifier.addRowToBody(
                    ProductsTableModifier.generateTableRow(
                        quoteItem
                    )
                );

                triggerModal();
                push_notification("Product added.", "The quote product has been succesfully added.", "success");
            },
            error: function () {
                push_notification("Product not found.", "The product was not find in the database.", "error");
            }
        });
    });
});
</script>
<!-- /Autocomplete -->
@stop
