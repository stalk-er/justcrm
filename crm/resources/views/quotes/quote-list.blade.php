@extends('master')
@section('content')

<div class="">
    <div class="page-title">
        <div class="title_left">&nbsp;</div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>quotes</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a></li>
                                <li><a href="#">Settings 2</a></li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped quotes">
                        <thead>
                            <tr>
                                <th style="width: 1%">#</th>
                                <th style="width: 40%">Name</th>
                                <th>Client</th>
                                <th style="width: 20%">#Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($quotes) > 0)
                            @foreach($quotes as $quote)
                            <tr>
                                <td>#</td>
                                <td>
                                    <a>{!! $quote->name !!}</a>
                                    <br />
                                    <small>{!! $quote->created_at !!}</small>
                                </td>
                                <td>{{ $quote->client }}</td>
                                <td>
                                    <a href="/quotes/{!! $quote->id !!}" data-id="{!! $quote->id !!}" class="btn btn-primary btn-xs preview"><i class="fa fa-folder"></i> View </a>
                                    <a href="/quotes/{!! $quote->id !!}/edit" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                    <a href="/quotes/{!! $quote->id !!}" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Print </a>
                                    {{ Form::open(array('url' => 'quotes/' . $quote->id, 'style' => 'display: inline-block; vertical-align: middle;')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs delete')) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="3">There's no quotes created yet</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
