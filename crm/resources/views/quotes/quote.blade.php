@extends("master")

@section("content")
{{ dd($template) }}
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Quote</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a></li>
                            <li><a href="#">Settings 2</a></li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <section class="content invoice">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-xs-12 invoice-header">
                            <h1>
                                <i class="fa fa-globe"></i> {{ $quote->name }}
                                <small class="pull-right">Date: {{ $quote->updated_at }}</small>
                            </h1>
                        </div>
                        <!-- /.col -->
                    </div>
                    <br>
                    <br>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">From <address> <strong>My company</strong> </address></div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            To
                            <address>
                                <strong> {{ $quote->client }} </strong>
                            </address>
                        </div>
                        <!-- /.col -->

                        <div class="col-sm-4 invoice-col">
                            Quote ID <strong>{!! $quote->unique_id !!}</strong>
                        </div>
                        <!-- /.col -->
                    </div>
                    <br>
                    <br>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-xs-12 table">
                            <table class="table table-striped" id="quoteProductsList">
                                <thead>
                                    <tr>
                                        <th>Product name</th>
                                        <th>Quantity</th>
                                        <th>Unit price</th>
                                        <th>Sub total</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php $itemsTotal = 0; ?>
                                    @if(isset($quoteItems))
                                    <?php $itemCounter = 0; ?>
                                    @foreach($quoteItems as $item)
                                    <tr>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->quantity }}</td>
                                        <td>{{ $item->unit_price }}</td>
                                        <td class="subtotal">{{ $item->unit_price * $item->quantity }}</td>
                                        <?php $itemsTotal +=  $item->unit_price * $item->quantity; ?>
                                    </tr>
                                    <?php $itemCounter++; ?>
                                    @endforeach
                                    @endif
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <td colspan="4" style="text-align:right;">Total &nbsp;
                                            <span id="quoteTotal">
                                                @if(isset($quote))
                                                {{ $itemsTotal }}
                                                @else
                                                {{ 0 }}
                                                @endif
                                            </span>
                                        </td>
                                        <td><strong></strong></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-6">
                            <h2 class="lead">Additional comments</h2>
                            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                {{ $quote->content }}
                            </p>
                            <p class="lead">Available Payment Methods:</p>
                            <img src="{!! URL::asset('assets/images/visa.png') !!}" alt="Visa" />
                            <img src="{!! URL::asset('assets/images/mastercard.png') !!}" alt="Mastercard" />
                            <img src="{!! URL::asset('assets/images/american-express.png') !!}" alt="American Express" />
                            <img src="{!! URL::asset('assets/images/paypal2.png') !!}" alt="Paypal" />
                            <br>
                            <br>
                        </div>

                        <!-- /.col -->
                        <div class="col-xs-6">
                            <p class="lead">Amount Due 2/22/2014</p>
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th style="width:30%">Subtotal:</th>
                                            <td></td>
                                            <td id="quoteSubTotal">{{ $itemsTotal }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%">Tax (20%)</th>
                                            <td></td>
                                            <?php
                                                $taxation = 20 / 100 * $itemsTotal;
                                            ?>
                                            <td data-tax="20" id="quoteTax">{{ $taxation }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%">Shipping:</th>
                                            <td>
                                                <?php $shipmentPrice = $quote->shipmentType->price; ?>
                                            </td>
                                            <td id="quoteShipping">{{ $shipmentPrice }}</td>
                                        </tr>
                                        <tr>
                                            <th width="30%">Total:</th>
                                            <td></td>
                                            <td id="quoteTotalWithTax">{{ $shipmentPrice + $itemsTotal + $taxation }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <a href="#" onclick="window.print();return false;" class="btn btn-info btn-md">Print</a>
                        <a href="{{ url('quotes/' . $quote->id . '/edit') }}" class="btn btn-warning btn-md">Edit</a>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
            </div>
        </div>
    </div>
</div>


<!-- Print -->
<script type="text/javascript">
jQuery(document).ready(function ($) {
    $(window).load(window.print);
});
</script>
<!-- /Print -->
@stop
