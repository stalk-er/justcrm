<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Registration verification</h2>
        <div>
            Thanks for creating an account in our new Custom Relationship Management System.
            Please follow the link below to verify your email address:
            {!! URL::to('register/verify/' . $confirmation_code) !!}.<br/>
            <br>
            Cheers!
        </div>
    </body>
</html>