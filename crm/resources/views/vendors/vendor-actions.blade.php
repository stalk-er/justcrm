@extends("master")

@section("content")
<div class="row">
    @if(isset($vendor))
        {{ Form::model($vendor, ['route' => ['vendors.update', $vendor->id], 'method' => 'PUT', 'class' => 'form-horizontal form-label-left']) }}
    @else
        {{ Form::open(['url' => 'vendors', 'class' => 'form-horizontal form-label-left']) }}
    @endif
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Create/Edit Vendor</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a></li>
                            <li><a href="#">Settings 2</a></li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="col-md-6">
                    <div class="x_title">
                        <h2>Vendor base information <small> name, vendor, prices</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <br>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Vendor name</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter name']) }}
                            @if ($errors->has('name'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Vendor description <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Enter description']) }}
                            @if ($errors->has('description'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                            @endif
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'Enter city']) }}
                            @if ($errors->has('city'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                            @endif
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::text('country', null, ['class' => 'form-control', 'placeholder' => 'Enter country']) }}
                            @if ($errors->has('country'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('country') }}</strong>
                            </span>
                            @endif
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Enter Address']) }}
                            @if ($errors->has('address'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                            @endif
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Zip code</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::text('zip_code', null, ['class' => 'form-control', 'placeholder' => 'Enter zip code']) }}
                            @if ($errors->has('zip_code'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('zip_code') }}</strong>
                            </span>
                            @endif
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter email']) }}
                            <!-- <input type="email" class="form-control"
                                   placeholder="Email"
                                   name="email"
                                   value="{!! (isset($vendor)) ? $vendor->email : '' !!}"
                            /> -->
                            @if ($errors->has('email'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="x_title">
                        <h2>Events <small> delivery date, status, expiry date, lot number</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <br>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::text('telephone', null, ['class' => 'form-control', 'placeholder' => 'Enter telephone']) }}
                            @if ($errors->has('telephone'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('telephone') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Fax</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::text('fax', null, ['class' => 'form-control', 'placeholder' => 'Enter fax']) }}
                            @if ($errors->has('fax'))
                            <span class="red help-block">
                                <strong>{{ $errors->first('fax') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Website</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::text('website', null, ['class' => 'form-control', 'placeholder' => 'Enter website']) }}
                            @if ($errors->has('website'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('website') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Image</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <input class="form-control" type="text" id="vendor-image" readonly name="avatar_image" value="{{ (isset($vendor))?$vendor->avatar_image:'' }}">
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <a href="" class="popup_selector btn btn-sm btn-info" data-inputid="vendor-image">Pick a file</a>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12" style="height: 150px; width: 150px; overflow: hidden;">
                            @if(isset($vendor))
                            <img style="max-width: 100%; width: auto; height: auto;" id="productImage" src="/{{ $vendor->avatar_image }}" alt="{{ $vendor->name }}">
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                        <a href="{{ URL::previous() }}" class="btn btn-primary">Cancel</a>
                        <input type="submit" value="{!! (isset($vendor)) ? 'Save' : 'Save' !!}" class="btn btn-success" />
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>

<!-- Date picker -->
<script type="text/javascript">
$(document).ready(function () {
    $('#single_cal1').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        locale: {
            format: "DD/MM/YYYY"
        },
    }, function (start, end, label) {

        $("#delivery_date_value").val(start.toISOString());
    });
    $('#single_cal2').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        locale: {
            format: "DD/MM/YYYY"
        },
    }, function (start, end, label) {

        $("#expiry_date_value").val(start.toISOString());
    });
});
</script>
<!-- /Date picker -->
@stop
