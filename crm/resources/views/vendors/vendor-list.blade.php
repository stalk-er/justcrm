@extends('master')
@section('content')

<div class="">
    <div class="page-title">
        <div class="title_left">&nbsp;</div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Vendors</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a></li>
                                <li><a href="#">Settings 2</a></li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped vendors">
                        <thead>
                            <tr>
                                <th style="width: 1%">#</th>
                                <th style="width: 40%">Vendor Name</th>
                                <th>Vendor Photo</th>
                                <th style="width: 20%">#Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($vendors as $vendor)
                            <tr>
                                <td>#</td>
                                <td>
                                    <a>{!! $vendor->name !!}</a>
                                    <br />
                                    <small>{!! $vendor->created_at !!}</small>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <li> <img src="{{ $vendor->avatar_image }}" class="avatar" alt="$vendor->name"> </li>
                                    </ul>
                                </td>
                                <td>
                                    <a href="/vendors/{!! $vendor->id !!}/preview" data-id="{!! $vendor->id !!}" class="btn btn-primary btn-xs preview"><i class="fa fa-folder"></i> View </a>
                                    <a href="/vendors/{!! $vendor->id !!}/edit" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                    {{ Form::open(array('url' => 'vendors/' . $vendor->id, 'style' => 'display: inline-block; vertical-align: middle;')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs delete')) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
