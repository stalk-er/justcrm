@extends('master')

@section('content')
<script type="text/javascript">  var token = "{{ Session::getToken() }}";</script>
<!-- editor -->
<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
<link href="{!! URL::asset('assets/css/editor/external/google-code-prettify/prettify.css') !!}" rel="stylesheet">
<link href="{!! URL::asset('assets/css/editor/index.css') !!}" rel="stylesheet">
<style>
    #editor { 
        min-height: 200px !important; 
        height: 200px !important; 
    }
    .editor-wrapper { display: none; }
    .message_wrapper { padding: 0px 10px;
                       margin: 0;
                       border-left: 5px solid #eee; }
    .message_wrapper blocquote { border: none; }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{!! $event->title !!}</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a href="#"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a></li>
                            <li><a href="#">Settings 2</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <section class="panel" id="event" data-id="{!! $event->id !!}">
                        <div class="x_title">
                            <h2>What's this event about ?</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
<!--                            <h3 class="green"><i class="fa fa-paint-brush"></i> Gentelella</h3>-->
                            <p>{!! $event->content !!}</p>
                            <hr>
                            <div class="project_detail">
                                <p class="title">Author</p>
                                <p>{!! $event->author !!}</p>
                            </div>
                            <hr>
                            <h4>Attachments</h4>
                            <ul class="list-unstyled project_files">
                                <li><a href=""><i class="fa fa-file-word-o"></i> Functional-requirements.docx</a></li>
                                <li><a href=""><i class="fa fa-file-pdf-o"></i> UAT.pdf</a></li>
                                <li><a href=""><i class="fa fa-mail-forward"></i> Email-from-flatbal.mln</a></li>
                                <li><a href=""><i class="fa fa-picture-o"></i> Logo.png</a></li>
                                <li><a href=""><i class="fa fa-file-word-o"></i> Contract-10_12_2014.docx</a></li>
                            </ul>
                            <div class="text-center mtop20">
                                <a href="#" class="btn btn-sm btn-primary">Add files</a>
                                <a href="#" class="btn btn-sm btn-warning">Report contact</a>
                                <a href="#" class="btn btn-sm btn-success" id="show-form">Add comment</a>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div>
                        <h4>Activity</h4>
                        <div class="editor-wrapper">
                            <div id="alerts"></div>
                            <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa icon-font"></i><b class="caret"></b></a>
                                    <ul class="dropdown-menu"></ul>
                                </div>
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a data-edit="fontSize 5"><p style="font-size:17px">Huge</p></a></li>
                                        <li><a data-edit="fontSize 3"><p style="font-size:14px">Normal</p></a></li>
                                        <li><a data-edit="fontSize 1"><p style="font-size:11px">Small</p></a></li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
                                    <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
                                    <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="icon-strikethrough"></i></a>
                                    <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="icon-list-ul"></i></a>
                                    <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="icon-list-ol"></i></a>
                                    <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
                                    <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="icon-indent-right"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
                                    <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
                                    <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
                                    <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="icon-link"></i></a>
                                    <div class="dropdown-menu input-append">
                                        <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                                        <button class="btn" type="button">Add</button>
                                    </div>
                                    <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="icon-cut"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="icon-picture"></i></a>
                                    <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
                                    <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
                                </div>
                            </div>
                            <div id="editor"></div>
                            <textarea name="descr" id="descr" style="display:none;"></textarea>
                            <div style="padding: 25px 0 0 0;">
                                <button class="btn btn-success btn-lg" id="post-comment">Send</button>
                            </div>
                        </div>
                        <hr>
                        <ul class="messages comments">
                            @foreach($event->comments() as $comment)
                            <li class="comment" data-id="{!! $comment->id !!}">
                                <img src="images/img.jpg" class="avatar" alt="Avatar">
                                <div class="message_date">
                                    <h3 class="date text-info">24</h3>
                                    <p class="month">May</p>
                                    <p class="year">1992</p>
                                </div>
                                <div class="message_wrapper">
                                    <!-- <h4 class="heading">Desmond Davison</h4> -->
                                    <div class="message">{!! $comment->content !!}</div>
                                    <br />
                                    <p class="url">
                                        <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                        <a href="#"><i class="fa fa-paperclip"></i> User Acceptance Test.doc </a>
                                    </p>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>
<input type="hidden" value="{!! $event->id !!}" id="event_id" />
<!-- editor -->
<script>
    $(document).ready(function () {
        $('.xcxc').click(function () {
            $('#descr').val($('#editor').html());
        });
        // Showing comment form
        $("#show-form").click(function (e) {
            e.preventDefault();
            $(".editor-wrapper").slideDown();
            return false;
        });
        function render_comment(obj) {
            
            $("ul.comments")
                    .prepend(
                            $("<li>").attr("class", "comment").attr("data-id", obj.id)
                            .append($("<img>").attr("src", "images/img.jpg").attr("alt", "Avatar"))
                            .append($("<div>").attr("class", "message_date")
                                    .append($("<h3>").attr("class", "date text-info").text("24"))
                                    .append($("<p>").attr("class", "month").text("May"))
                                    .append($("<p>").attr("class", "year").text("1992"))
                                    )
                            .append(
                                    $("<div>").attr("class", "message_wrapper")
                                    .append(
                                            $("<div>").attr("class", "message").text(obj.content)
                                            )
                                    )
                            );
        }

        function render_latest(data) {
            if (data === "")
                return false;
            data = JSON.parse(data);
            for (var key in data) {
                render_comment(data[key]);
            }
        }

        function fetch_latest(lid) {
            $.ajax({
                type: "POST",
                url: window.location.origin + "/events/fetch-comments",
                processData: true,
                data: {
                    _token: token,
                    data_refresh: {
                        last_comment_id: lid,
                        event_id: parseInt($("#event_id").val())
                    }
                },
                beforeSend: function (xhr) {
                    console.log("sending data...");
                },
                success: function (data, textStatus, jqXHR) {

                    render_latest(data);
                }
            });
        }
        // Fetching latest comments each X seconds
        setInterval(function () {
            var lid = parseInt($(".comment").first().data("id"));
            fetch_latest(lid);
        }, 7 * 1000);
        function post_comment() {

            $("#post-comment").click(function () {
                if ($("#editor").text() == "") {
                    new PNotify({
                        title: 'Warning',
                        text: 'Please write a comment before sending it.',
                        type: 'warning',
                        hide: true
                    });
                    return false;
                }
                save_comment();
            });
        }
        post_comment();
        function save_comment() {

            var author = "{{ Auth::user()->id }}";
            var content = $('#editor').html();
            $.ajax({
                type: "POST",
                url: window.location.origin + "/events/comment/save",
                processData: true,
                data: {
                    _token: token,
                    data: {
                        id: parseInt($("#event").data("id")),
                        content: content,
                        author: author,
                        event: parseInt($("#event_id").val())
                    }
                },
                beforeSend: function (xhr) {
                    $("#post-comment").prop("disabled", true).text("Sending...");
                },
                success: function (data, textStatus, jqXHR) {
                    $("#post-comment").prop("disabled", false).text("Send");
                    $("#editor").empty();
                    new PNotify({
                        title: 'Success',
                        text: 'Comment was successfuly sended.',
                        type: 'success',
                        hide: true
                    });
                    render_comment({
                        id: parseInt($("#event").data("id")),
                        content: content
                    });
                }
            });
        }
    });
    $(function () {
        function initToolbarBootstrapBindings() {
            var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                'Times New Roman', 'Verdana'],
                    fontTarget = $('[title=Font]').siblings('.dropdown-menu');
            $.each(fonts, function (idx, fontName) {
                fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
            });
            $('a[title]').tooltip({
                container: 'body'
            });
            $('.dropdown-menu input').click(function () {
                return false;
            })
                    .change(function () {
                        $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                    })
                    .keydown('esc', function () {
                        this.value = '';
                        $(this).change();
                    });
            $('[data-role=magic-overlay]').each(function () {
                var overlay = $(this),
                        target = $(overlay.data('target'));
                overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
            });
            if ("onwebkitspeechchange" in document.createElement("input")) {
                var editorOffset = $('#editor').offset();
                $('#voiceBtn').css('position', 'absolute').offset({
                    top: editorOffset.top,
                    left: editorOffset.left + $('#editor').innerWidth() - 35
                });
            } else {
                $('#voiceBtn').hide();
            }
        }
        ;
        function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
                msg = "Unsupported format " + detail;
            } else {
                console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }
        ;
        initToolbarBootstrapBindings();
        $('#editor').wysiwyg({
            fileUploadError: showErrorAlert
        });
        window.prettyPrint && prettyPrint();
    });
</script>
<!-- richtext editor -->
<script src="{!! URL::asset('assets/js/editor/bootstrap-wysiwyg.js') !!}"></script>
<script src="{!! URL::asset('assets/js/editor/external/jquery.hotkeys.js') !!}"></script>
<script src="{!! URL::asset('assets/js/editor/external/google-code-prettify/prettify.js') !!}"></script>

@stop