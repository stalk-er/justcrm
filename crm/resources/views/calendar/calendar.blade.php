@extends('master')

@section('content')
<style type="text/css">
    .dt-number { line-height: 0; }
    .dt-number-control { max-width: 150px; }
    .dt-number-control * { display: inline-block; }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Calender<small>Click to add/edit events</small></h3>
                    </div>
                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Calender Events <small>Sessions</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a></li>
                                            <li><a href="#">Settings 2</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div id='calendar'></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">New Calender Entry</h4>
                </div>
                <div class="modal-body">
                    <div id="testmodal" style="padding: 5px 20px;">
                        <form id="antoform" class="form-horizontal calender" role="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Title</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="title_new" name="title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">From</label>
                                <div class="col-sm-9">
                                    <select class="choose_date_from form-control">
                                        <option value="12:00AM">12:00 AM</option>
                                        <option value="12:30AM">12:30 AM</option>
                                        <option value="1:00AM">1:00 AM</option>
                                        <option value="1:30AM">1:30 AM</option>
                                        <option value="2:00AM">2:00 AM</option>
                                        <option value="2:30AM">2:30 AM</option>
                                        <option value="3:00AM">3:00 AM</option>
                                        <option value="3:30AM">3:30 AM</option>
                                        <option value="4:00AM">4:00 AM</option>
                                        <option value="4:30AM">4:30 AM</option>
                                        <option value="5:00AM">5:00 AM</option>
                                        <option value="5:30AM">5:30 AM</option>
                                        <option value="6:00AM">6:00 AM</option>
                                        <option value="6:30AM">6:30 AM</option>
                                        <option value="7:00AM">7:00 AM</option>
                                        <option value="7:30AM">7:30 AM</option>
                                        <option value="8:00AM">8:00 AM</option>
                                        <option value="8:30AM">8:30 AM</option>
                                        <option value="9:00AM">9:00 AM</option>
                                        <option value="9:30AM">9:30 AM</option>
                                        <option value="10:00AM">10:00 AM</option>
                                        <option value="10:30AM">10:30 AM</option>
                                        <option value="11:00AM">11:00 AM</option>
                                        <option value="11:30AM">11:30 AM</option>
                                        <option value="12:00PM">12:00 PM</option>
                                        <option value="12:30PM">12:30 PM</option>
                                        <option value="1:00PM">1:00 PM</option>
                                        <option value="1:30PM">1:30 PM</option>
                                        <option value="2:00PM">2:00 PM</option>
                                        <option value="2:30PM">2:30 PM</option>
                                        <option value="3:00PM">3:00 PM</option>
                                        <option value="3:30PM">3:30 PM</option>
                                        <option value="4:00PM">4:00 PM</option>
                                        <option value="4:30PM">4:30 PM</option>
                                        <option value="5:00PM">5:00 PM</option>
                                        <option value="5:30PM">5:30 PM</option>
                                        <option value="6:00PM">6:00 PM</option>
                                        <option value="6:30PM">6:30 PM</option>
                                        <option value="7:00PM">7:00 PM</option>
                                        <option value="7:30PM">7:30 PM</option>
                                        <option value="8:00PM">8:00 PM</option>
                                        <option value="8:30PM">8:30 PM</option>
                                        <option value="9:00PM">9:00 PM</option>
                                        <option value="9:30PM">9:30 PM</option>
                                        <option value="10:00PM">10:00 PM</option>
                                        <option value="10:30PM">10:30 PM</option>
                                        <option value="11:00PM">11:00 PM</option>
                                        <option value="11:30PM">11:30 PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">To</label>
                                <div class="col-sm-9">
                                    <select class="choose_date_to form-control">
                                        <option value="null">-- None --</option>
                                        <option value="12:00AM">12:00 AM</option>
                                        <option value="12:30AM">12:30 AM</option>
                                        <option value="1:00AM">1:00 AM</option>
                                        <option value="1:30AM">1:30 AM</option>
                                        <option value="2:00AM">2:00 AM</option>
                                        <option value="2:30AM">2:30 AM</option>
                                        <option value="3:00AM">3:00 AM</option>
                                        <option value="3:30AM">3:30 AM</option>
                                        <option value="4:00AM">4:00 AM</option>
                                        <option value="4:30AM">4:30 AM</option>
                                        <option value="5:00AM">5:00 AM</option>
                                        <option value="5:30AM">5:30 AM</option>
                                        <option value="6:00AM">6:00 AM</option>
                                        <option value="6:30AM">6:30 AM</option>
                                        <option value="7:00AM">7:00 AM</option>
                                        <option value="7:30AM">7:30 AM</option>
                                        <option value="8:00AM">8:00 AM</option>
                                        <option value="8:30AM">8:30 AM</option>
                                        <option value="9:00AM">9:00 AM</option>
                                        <option value="9:30AM">9:30 AM</option>
                                        <option value="10:00AM">10:00 AM</option>
                                        <option value="10:30AM">10:30 AM</option>
                                        <option value="11:00AM">11:00 AM</option>
                                        <option value="11:30AM">11:30 AM</option>
                                        <option value="12:00PM">12:00 PM</option>
                                        <option value="12:30PM">12:30 PM</option>
                                        <option value="1:00PM">1:00 PM</option>
                                        <option value="1:30PM">1:30 PM</option>
                                        <option value="2:00PM">2:00 PM</option>
                                        <option value="2:30PM">2:30 PM</option>
                                        <option value="3:00PM">3:00 PM</option>
                                        <option value="3:30PM">3:30 PM</option>
                                        <option value="4:00PM">4:00 PM</option>
                                        <option value="4:30PM">4:30 PM</option>
                                        <option value="5:00PM">5:00 PM</option>
                                        <option value="5:30PM">5:30 PM</option>
                                        <option value="6:00PM">6:00 PM</option>
                                        <option value="6:30PM">6:30 PM</option>
                                        <option value="7:00PM">7:00 PM</option>
                                        <option value="7:30PM">7:30 PM</option>
                                        <option value="8:00PM">8:00 PM</option>
                                        <option value="8:30PM">8:30 PM</option>
                                        <option value="9:00PM">9:00 PM</option>
                                        <option value="9:30PM">9:30 PM</option>
                                        <option value="10:00PM">10:00 PM</option>
                                        <option value="10:30PM">10:30 PM</option>
                                        <option value="11:00PM">11:00 PM</option>
                                        <option value="11:30PM">11:30 PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" style="height:55px;" id="description_new" name="descr"></textarea>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="col-sm-3 control-label">All day</label>
                                <div class="col-sm-9">
                                    <input class="flat" type="checkbox" class="form-control" name="allDay" id="allDay" />
                                </div>
                            </div> -->
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary antosubmit">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel2">Edit Calender Entry</h4>
                </div>
                <div class="modal-body">

                    <div id="testmodal2" style="padding: 5px 20px;">
                        <form id="antoform2" class="form-horizontal calender" role="form">
                            <input type="hidden" value="" id="eventID" />
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button id="deleteEvent" class="btn btn-danger btn-xs">DELETE</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Title</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="title_edit" name="title2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">From</label>
                                <div class="col-sm-9">
                                    <select class="choose_date_from form-control">
                                        <option value="12:00AM">12:00 AM</option>
                                        <option value="12:30AM">12:30 AM</option>
                                        <option value="1:00AM">1:00 AM</option>
                                        <option value="1:30AM">1:30 AM</option>
                                        <option value="2:00AM">2:00 AM</option>
                                        <option value="2:30AM">2:30 AM</option>
                                        <option value="3:00AM">3:00 AM</option>
                                        <option value="3:30AM">3:30 AM</option>
                                        <option value="4:00AM">4:00 AM</option>
                                        <option value="4:30AM">4:30 AM</option>
                                        <option value="5:00AM">5:00 AM</option>
                                        <option value="5:30AM">5:30 AM</option>
                                        <option value="6:00AM">6:00 AM</option>
                                        <option value="6:30AM">6:30 AM</option>
                                        <option value="7:00AM">7:00 AM</option>
                                        <option value="7:30AM">7:30 AM</option>
                                        <option value="8:00AM">8:00 AM</option>
                                        <option value="8:30AM">8:30 AM</option>
                                        <option value="9:00AM">9:00 AM</option>
                                        <option value="9:30AM">9:30 AM</option>
                                        <option value="10:00AM">10:00 AM</option>
                                        <option value="10:30AM">10:30 AM</option>
                                        <option value="11:00AM">11:00 AM</option>
                                        <option value="11:30AM">11:30 AM</option>
                                        <option value="12:00PM">12:00 PM</option>
                                        <option value="12:30PM">12:30 PM</option>
                                        <option value="1:00PM">1:00 PM</option>
                                        <option value="1:30PM">1:30 PM</option>
                                        <option value="2:00PM">2:00 PM</option>
                                        <option value="2:30PM">2:30 PM</option>
                                        <option value="3:00PM">3:00 PM</option>
                                        <option value="3:30PM">3:30 PM</option>
                                        <option value="4:00PM">4:00 PM</option>
                                        <option value="4:30PM">4:30 PM</option>
                                        <option value="5:00PM">5:00 PM</option>
                                        <option value="5:30PM">5:30 PM</option>
                                        <option value="6:00PM">6:00 PM</option>
                                        <option value="6:30PM">6:30 PM</option>
                                        <option value="7:00PM">7:00 PM</option>
                                        <option value="7:30PM">7:30 PM</option>
                                        <option value="8:00PM">8:00 PM</option>
                                        <option value="8:30PM">8:30 PM</option>
                                        <option value="9:00PM">9:00 PM</option>
                                        <option value="9:30PM">9:30 PM</option>
                                        <option value="10:00PM">10:00 PM</option>
                                        <option value="10:30PM">10:30 PM</option>
                                        <option value="11:00PM">11:00 PM</option>
                                        <option value="11:30PM">11:30 PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">To</label>
                                <div class="col-sm-9">
                                    <select class="choose_date_to form-control">
                                        <option value="null">-- None --</option>
                                        <option value="12:00AM">12:00 AM</option>
                                        <option value="12:30AM">12:30 AM</option>
                                        <option value="1:00AM">1:00 AM</option>
                                        <option value="1:30AM">1:30 AM</option>
                                        <option value="2:00AM">2:00 AM</option>
                                        <option value="2:30AM">2:30 AM</option>
                                        <option value="3:00AM">3:00 AM</option>
                                        <option value="3:30AM">3:30 AM</option>
                                        <option value="4:00AM">4:00 AM</option>
                                        <option value="4:30AM">4:30 AM</option>
                                        <option value="5:00AM">5:00 AM</option>
                                        <option value="5:30AM">5:30 AM</option>
                                        <option value="6:00AM">6:00 AM</option>
                                        <option value="6:30AM">6:30 AM</option>
                                        <option value="7:00AM">7:00 AM</option>
                                        <option value="7:30AM">7:30 AM</option>
                                        <option value="8:00AM">8:00 AM</option>
                                        <option value="8:30AM">8:30 AM</option>
                                        <option value="9:00AM">9:00 AM</option>
                                        <option value="9:30AM">9:30 AM</option>
                                        <option value="10:00AM">10:00 AM</option>
                                        <option value="10:30AM">10:30 AM</option>
                                        <option value="11:00AM">11:00 AM</option>
                                        <option value="11:30AM">11:30 AM</option>
                                        <option value="12:00PM">12:00 PM</option>
                                        <option value="12:30PM">12:30 PM</option>
                                        <option value="1:00PM">1:00 PM</option>
                                        <option value="1:30PM">1:30 PM</option>
                                        <option value="2:00PM">2:00 PM</option>
                                        <option value="2:30PM">2:30 PM</option>
                                        <option value="3:00PM">3:00 PM</option>
                                        <option value="3:30PM">3:30 PM</option>
                                        <option value="4:00PM">4:00 PM</option>
                                        <option value="4:30PM">4:30 PM</option>
                                        <option value="5:00PM">5:00 PM</option>
                                        <option value="5:30PM">5:30 PM</option>
                                        <option value="6:00PM">6:00 PM</option>
                                        <option value="6:30PM">6:30 PM</option>
                                        <option value="7:00PM">7:00 PM</option>
                                        <option value="7:30PM">7:30 PM</option>
                                        <option value="8:00PM">8:00 PM</option>
                                        <option value="8:30PM">8:30 PM</option>
                                        <option value="9:00PM">9:00 PM</option>
                                        <option value="9:30PM">9:30 PM</option>
                                        <option value="10:00PM">10:00 PM</option>
                                        <option value="10:30PM">10:30 PM</option>
                                        <option value="11:00PM">11:00 PM</option>
                                        <option value="11:30PM">11:30 PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" style="height:55px;" id="description_edit" name="descr"></textarea>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="col-sm-3 control-label">All day</label>
                                <div class="col-sm-9">
                                    <input class="flat" type="checkbox" class="form-control" name="allDayEdit" id="allDayEdit" />
                                </div>
                            </div> -->
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default antoclose2" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary antosubmit2">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
    <div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>

    <script type="text/javascript">
        var formated_date;

        $(window).on("load", fetch_events);

        function fetch_events() {
            $.ajax({
                url: window.location.origin + "/calendar/events/get",
                type: "GET",
                dataType: "json",
                success: function (data) {
                    data = boolean_parse(data);
                    init_calender(data);
                }
            });
        }

        function boolean_parse(collection) {

            for (var i = 0; i < collection.length; i++) {
                if (collection[i].allDay == 0) {
                    collection[i].allDay = false;
                }
                else {
                    collection[i].allDay = true;
                }
            }
            return collection;
        }

        function deleteEvent(id) {

            var token = "{{ Session::getToken() }}";
            //data = JSON.parse(data);
            return $.ajax({
                url: window.location.origin + "/calendar/event/delete",
                type: "post",
                data: {
                    _token: token,
                    id: id,
                }
            });
        }

        function save_event(data) {

            var token = "{{ Session::getToken() }}";
            //data = JSON.parse(data);
            return $.ajax({
                url: window.location.origin + "/calendar/event/save",
                type: "post",
                data: {
                    _token: token,
                    event: data,
                }
            });
        }

        function update_event(data) {

            var token = "{{ Session::getToken() }}";
            //data = JSON.parse(data);
            return $.ajax({
                url: window.location.origin + "/calendar/event/update",
                type: "post",
                data: {
                    _token: token,
                    event: data,
                }
            });
        }

        function convert_from_12_to_24(date, date_object) {

            if(typeof date == "undefined" || date == "null" || date == null) {
                return null;
            }

            var ampm = date.toString().slice(date.length - 2, date.length).toLowerCase();
            date = date.toString().slice(0, date.length - 2).split(":");
            if (ampm == "pm") {
                date[0] = (parseInt(date[0]) + 12).toString();
            }
            else {
                if (date[0].length < 2) {
                    date[0] = ("0" + "" + date[0]).toString();
                }
            }
            date_object.setHours(date[0]);
            date_object.setMinutes(date[1]);

            var s = date_object.toString().indexOf('(');

            result = date_object.toString().slice(0, s - 1);
            return result;
        }

        function cut_locate_string(date) {
            var s = date.toString().indexOf('(');
            return date.toString().slice(0, s - 1);
        }

        function formatAMPM(date) {

            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + '' + ampm;

            return strTime;
        }

        function get_day_events(date, events) {

            var dayEvents = [];
            for(var e in events) {
                if(events[e].start == date.getTime())
                    dayEvents.push(events[e]);
            }
            return dayEvents;
        }

        function isThereAllDayEvents(events) {

            var isThereAllDayEvents = false;
            for(var e in events) {
                if(events[e].allDay == true) {
                    isThereAllDayEvents = true;
                    break;
                }
            }
            return isThereAllDayEvents;
        }

        function init_calender(events) {

            var calendar = $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month, agendaWeek, agendaDay'
                },
                timezone: "local",
                timeFormat: 'H(:mm)', // uppercase H for 24-hour clock
                selectable: true,
                selectHelper: false,
                dayClick: function (date, allDay, jsEvent, view) {

                    // Open the create dialog window
                    $('#fc_create').click();

                    // On save
                    $(".antosubmit").unbind("click").on("click", function () {

                        var title = $("#title_new").val();
                        var description = $("#description_new").val();
                        var from = $("#CalenderModalNew .choose_date_from").val();
                        var to = $("#CalenderModalNew .choose_date_to").val();
                        var isAllDay = $("#CalenderModalNew #allDay").is(':checked');
                        var allEvents = calendar.fullCalendar('clientEvents');
                        var dayEvents = get_day_events(new Date(date._d), allEvents);

                        if(!title.trim()) {
                            push_notification("Validation Error", "Title cannot be empty. Please enter a title.", "error");
                            return false;
                        }

                        if(isThereAllDayEvents(dayEvents)) { // returns true if all day events exist
                            push_notification("All day event exists", "Keep in mind that you already have a all day event on that day.", "warning");
                        }

                        from = convert_from_12_to_24(from, new Date(date._d));
                        to = convert_from_12_to_24(to, new Date(date._d));

                        save_event({
                            title: title,
                            start: moment(from).toString(),
                            end: moment(to).toString(),
                            description: description,
                        })
                        .done(function (eventObj) { // save data to db
                            calendar.fullCalendar('renderEvent', eventObj, true); // make the event "stick"
                            push_notification("Event added", "The event was successfully added.", "success");
                        });

                        $('.antoclose').click();
                        $(".antosubmit").unbind("click");
                        calendar.fullCalendar('unselect');
                    });

                    // Close the modal dialog
                    var modalID = $('#fc_create').attr("data-target");
                    //$(modalID).bind("hidden.bs.modal");
                    $(modalID).unbind("hidden.bs.modal").on('hidden.bs.modal', function () {

                        // Empty the fields on close
                        $('#title_new').val('');
                        $('#description_new').val('');
                        $('#allDay').iCheck('uncheck');
                        $(".choose_date_to").val("null");
                        $(modalID).unbind("hidden.bs.modal");
                    });

                },
                eventClick: function (calEvent, jsEvent, view) {

                    // Opening edit dialog window
                    $('#fc_edit').click();
                    $("#eventID").val(calEvent.id);

                    var modalID = $('#fc_edit').attr("data-target");
                    var started;
                    var ended;

                    if(calEvent.start) {
                        started = formatAMPM(
                            new Date(calEvent.start)
                        ).toUpperCase();
                    }

                    // Ended
                    if(calEvent.end) {
                        ended = formatAMPM(
                            new Date(calEvent.end)
                        ).toUpperCase();
                    }

                    // Preselect fields
                    (started)?$(".choose_date_from").val(started):$(".choose_date_from").val("null");
                    (ended)?$(".choose_date_to").val(ended):$(".choose_date_to").val("null");


                    // This is the title
                    if(calEvent.title) {
                        $('#title_edit').val(calEvent.title)
                    }

                    // This is the description
                    if(calEvent.description) {
                        $('#description_edit').val(calEvent.description);
                    }

                    // This is the description
                    if(calEvent.allDay)
                    $('#allDayEdit').iCheck('check');

                    // Delete event
                    //$("#deleteEvent").bind("click");
                    $("#deleteEvent").unbind("click").on("click", function (e) {
                        e.preventDefault();

                        if(confirm("Are you sure you want to delete this event?")) {
                            var id = $("#eventID").val();
                            deleteEvent(id).done(function (message) {

                                push_notification("Event deleted", "The calender event was successfully deleted.", "success");
                                calendar.fullCalendar("removeEvents", id);
                            });

                            $(".antoclose2").click();
                        }

                        $("#deleteEvent").unbind("click");
                    });

                    // Save event
                    //$(".antosubmit2").bind("click");
                    $(".antosubmit2").unbind("click").on("click", function () {

                        var from = convert_from_12_to_24($(modalID).find(".choose_date_from").val(), new Date(calEvent.start._i));
                        var to = convert_from_12_to_24($(modalID).find(".choose_date_to").val(), new Date(calEvent.start._i));

                        calEvent.title = $("#title_edit").val();
                        calEvent.description = $("#description_edit").val();
                        calEvent.start = moment(from);
                        calEvent.end = moment(to);

                        update_event({
                            id: calEvent.id,
                            title: calEvent.title,
                            description: calEvent.description,
                            start: moment(from).toString(),
                            end: moment(to).toString(),
                        })
                        .done(function (eventObj) { // save data to db
                            calendar.fullCalendar('updateEvent', calEvent, true);
                            push_notification("Event added", "The event was successfully added.", "success");
                        });
                        $('.antoclose2').click();
                        $(".antosubmit2").unbind("click");
                    });

                    // Close the modal dialog
                    //$(modalID).bind("hidden.bs.modal");
                    $(modalID).unbind("hidden.bs.modal").on('hidden.bs.modal', function () {
                        $('#allDayEdit').iCheck('uncheck');
                        $(".choose_date_to").val("null");
                        $("#eventID").val("");
                        $(modalID).unbind("hidden.bs.modal");
                    });
                    calendar.fullCalendar('unselect');
                },
                eventResize: function (event, delta, revertFunc) {

                    push_notification("End date changed!", event.title + " end is now " + event.end.format(), "warning");

                    window.setTimeout(function () {
                        update_event({
                            id: event.id,
                            title: event.title,
                            description: event.description,
                            start: event.start.format(),
                            end: event.end.format()
                        })
                        .done(function (obj) {
                            push_notification("Event changed", "The event was edited successfully.", "success");
                        });
                    }, 2000);
                },
                eventDrop: function(event, delta, revertFunc) {

                    push_notification("Event dropped!", event.title + " was dropped on " + event.start.format(), "warning");

                    window.setTimeout(function () {
                        update_event({
                            id: event.id,
                            title: event.title,
                            description: event.description,
                            start: event.start.format(),
                            end: event.end.format()
                        })
                        .done(function (obj) {
                            push_notification("Event changed", "The event was edited successfully.", "success");
                        });
                    }, 2000);
                },
                editable: true,
                events: window.location.origin + "/calendar/events/get"
            });

        }
    </script>
    @stop
