@extends('master')

@section('content')
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Events</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a></li>
                                <li><a href="#">Settings 2</a></li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- start project list -->
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 1%">Title</th>
                                <th style="width: 20%">Overview</th>
<!--                                <th>Attachments</th>-->
                                <th>Status</th>
                                <th>Author</th>
                                <th style="width: 20%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($events as $event)
                            <tr data-id="{!! $event->id !!}">
                                <td>{!! $event->title !!}</td>
                                <td>
                                    <a>{!! $event->overview !!}</a>
                                    <br />
                                    <small>Created at {!! $event->created_at !!}</small>
                                </td>
<!--                                <td>
                                    <ul class="list-inline">
                                        <li>
                                            <img src="http://localhost/personal/njoroge/assets3/img.jpg" class="avatar" alt="Avatar">
                                        </li>
                                        <li>
                                            <img src="http://localhost/personal/njoroge/assets3/img.jpg" class="avatar" alt="Avatar">
                                        </li>
                                        <li>
                                            <img src="http://localhost/personal/njoroge/assets3/img.jpg" class="avatar" alt="Avatar">
                                        </li>
                                        <li>
                                            <img src="http://localhost/personal/njoroge/assets3/img.jpg" class="avatar" alt="Avatar">
                                        </li>
                                    </ul>
                                </td>-->
                                <td>
                                    <h3>{!! $event->status !!}</h3>
                                </td>
                                <td>{!! $event->author !!}</td>
                                <td>
                                    <button type="button" class="btn btn-success btn-lg view-event">View</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $(".view-event").click(function () {

            var id = parseInt($(this).parent().parent().data("id"));
            var url = window.location.origin + "/";

            if ($("body").find(".quick-preview[data-event-id='" + id + "']").length > 0) {
                $(".quick-preview[data-event-id='" + id + "']").modal("show");
                return false;
            }
            $.ajax({
                type: "GET",
                url: url + "events/" + id + "/details",
                success: function (data) {
                    renderModal($(data).find(".right_col"), id);
                }
            });
        });
    });



    function renderModal(data, event_id) {

        $("body").append(
                $("<div>").attr("class", "modal fade bs-example-modal-lg quick-preview")
                .attr("tabindex", "-1")
                .attr("role", "dialog")
                .attr("aria-hidden", "true")
                .attr("data-event-id", event_id)
                .append(
                        $("<div>").attr("class", "modal-dialog modal-lg")
                        .append(
                                $("<div>").attr("class", "modal-content")
                                .append(
                                        $("<div>").attr("class", "modal-header")
                                        .append(
                                                $("<button>").attr("type", "button").attr("class", "close").attr("data-dismiss", "modal").attr("aria-label", "Close").text("×")
                                                )
                                        .append(
                                                $("<h4>").attr("class", "modal-title").text("Quick Preview")
                                                )
                                        )
                                .append(
                                        $("<div>").attr("class", "modal-body")
                                        .append(data)
                                        )
                                .append(
                                        $("<div>").attr("class", "modal-footer")
                                        .append(
                                                $("<button>")
                                                .attr("type", "button")
                                                .attr("class", "btn btn-default")
                                                .attr("data-dismiss", "modal")
                                                .text("Close")
                                                )
                                        )
                                )
                        )
                );
        $("body").find(".quick-preview[data-event-id='" + event_id + "']").modal("show");
    }
</script>


@stop