<li role="presentation" class="dropdown">
    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-envelope-o"></i>
        <span class="badge bg-green" id="notificationsCount">{{ count($notifications) }}</span>
    </a>
    <ul id="notificationsMenu" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
        @if(count($notifications))
        @foreach($notifications as $notification)
        <li data-notification="{{ $notification->id }}">
            <a href="{{ $notification->action }}" target="_blank">
                <span class="image" style="padding: 15px;"><i class="fa fa-info" style="font-size: 13px;"></i></span>
                <span>
                    <span style="display: inline-block; width: 50%; margin-bottom: 10px;">{{ $notification->title }}</span>
                    <span class="time" style="margin: 0 0 20px 20px; display: inline-block;">{{
                        $notification->time_since(
                            strtotime(
                                $notification->created_at->format('Y-m-d H:i:s')
                            )
                        )
                    }} ago</span>
                </span>
                <span class="message">{!! $notification->content !!}<span>
                    <a href="" class="markRead">Mark read</a>
                </a>
            </li>
            @endforeach
            <li><a href="{{ url('/events') }}">See all</a></li>
            @else
            <li><a>No new events</a></li>
            <li><a href="{{ url('/events') }}">See all</a></li>
            @endif
        </ul>
    </li>
<script type="text/javascript">
    $(document).ready(function () {
        $("#notificationsMenu").find(".markRead").on("click", function (e) {
            e.preventDefault();

            var notificationId = $(this).closest("[data-notification]").data("notification");

            $.ajax({
                url: window.location.origin + "/mark-read",
                type: "POST",
                data: {
                    _token: $("meta[name='token']").attr("content"),
                    notificationId: notificationId
                },
                success: function () {
                    var notificationsCount = parseInt( $("#notificationsCount").text() ) - 1;
                    $("#notificationsCount").text(notificationsCount);
                }
            });
        });
    });
</script>
