@extends("master")

@section("content")
<div class="row">
    @if(isset($template))
    {{ Form::model($template, [
        'route' => ['quote-templates.update', $template->id],
        'method' => 'PUT',
        'class' => 'form-horizontal form-label-left',
        'files' => true
        ]) }}
    @else
    {{ Form::open([
        'url' => 'quote-templates',
        'class' => 'form-horizontal form-label-left',
        'files' => true
        ]) }}
        @endif
        {{ Form::hidden('itemsCount', (isset($templateItems)) ? count($templateItems) : 0, [
        "id" => "itemsCount"
        ]) }}
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Quote Template</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Preview</a></li>
                                <li><a href="#" onclick="window.print(); return false;">Print</a></li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <section class="content invoice">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-xs-12 invoice-header">
                                <h1>
                                    {{ Form::text('name', null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Enter quote name',
                                    'style' => 'width: 80%; display: inline-block; vertical-align: middle;'
                                    ])
                                }}
                            </h1>
                            @if ($errors->has('name'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <!-- /.col -->
                    </div>

                    <div class="row">


                        <div class="col-xs-12">
                            {{ Form::label("title_position", "Title position") }}
                            {{
                                Form::select('title_position', [
                                    "left" => "Display the title on the left",
                                    "right" => "Display the title on the right",
                                ], null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Title position',
                                    'required'
                                ])
                            }}
                            @if ($errors->has('title_position'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('title_position') }}</strong>
                            </span>
                            @endif
                        </div>


                        <div class="col-xs-12">
                            {{ Form::label("from_position", "FROM details position") }}
                            {{
                                Form::select('from_position', [
                                    "first" => "Display the FROM details first",
                                    "second" => "Display the FROM details second",
                                ], null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'FROM details position',
                                    'required'
                                ])
                            }}
                            @if ($errors->has('from_position'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('from_position') }}</strong>
                            </span>
                            @endif
                        </div>


                        <div class="col-xs-12">
                            {{ Form::label("to_position", "TO details position") }}
                            {{
                                Form::select('to_position', [
                                    "first" => "Display the TO details first",
                                    "second" => "Display the TO details second",
                                ], null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'TO details position',
                                    'required'
                                ])
                            }}
                            @if ($errors->has('to_position'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('to_position') }}</strong>
                            </span>
                            @endif
                        </div>


                        <div class="col-xs-12">
                            {{ Form::label("slogan_position", "Company Slogan position") }}
                            {{
                                Form::select('slogan_position', [
                                    "below" => "Display the Company slogan below the logo",
                                    "next" => "Display the Company slogan next to the logo",
                                ], null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Company Slogan position',
                                    'required'
                                ])
                            }}
                            @if ($errors->has('slogan_position'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('slogan_position') }}</strong>
                            </span>
                            @endif
                        </div>


                        <div class="col-xs-12">
                            {{ Form::label("date_position", "Date position") }}
                            {{
                                Form::select('slogan_position', [
                                    "left" => "Display the date on the left",
                                    "right" => "Display the date on the right",
                                ], null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Date position',
                                    'required'
                                ])
                            }}
                            @if ($errors->has('date_position'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('date_position') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="col-xs-12">
                            {{ Form::label("comments_show", "Display additional comments") }}
                            {{
                                Form::checkbox('comments_show', null, null, [
                                    'class' => 'js-switch',
                                    'placeholder' => 'Additional Comments position',
                                    'required'
                                ])
                            }}
                            @if ($errors->has('comments_position'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('comments_position') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="col-xs-12">
                            {{ Form::label("unique_number_show", "Display the item unique number") }}
                            {{
                                Form::checkbox('unique_number_show', null, null, [
                                    'class' => 'js-switch',
                                ])
                            }}
                            @if ($errors->has('unique_number_show'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('unique_number_show') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-xs-12">
                            {{ Form::label("slogan_show", "Display slogan") }}
                            {{
                                Form::checkbox('slogan_show', null, null, [
                                    'class' => 'js-switch',
                                ])
                            }}
                            @if ($errors->has('slogan_show'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('slogan_show') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-xs-12">
                            {{ Form::label("logo_show", "Display logo") }}
                            {{
                                Form::checkbox('logo_show', null, null, [
                                    'class' => 'js-switch',
                                ])
                            }}
                            @if ($errors->has('logo_show'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('logo_show') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-xs-12">
                            {{ Form::label("comments_show", "Display additional comments") }}
                            {{
                                Form::checkbox('comments_show', null, null, [
                                    'class' => 'js-switch',
                                    'placeholder' => 'Additional Comments position',
                                ])
                            }}
                            @if ($errors->has('comments_position'))
                            <span class="help-block red">
                                <strong>{{ $errors->first('comments_position') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <input type="submit" class="btn btn-success btn-lg" value="Save" />
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>


<div id="addQuoteProductModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">New Calender Entry</h4>
            </div>
            <div class="modal-body">
                <div id="testmodal" style="padding: 5px 20px;">
                    <form id="antoform" class="form-horizontal calender" role="form">
                        <div class="form-group">
                            <label class="col-sm-12 control-label">Type product name:</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="pName" name="pName" style="margin-top: 15px;">
                                <div id="autocomplete-container" style="position: relative; float: left; width: 400px; margin: 10px;"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary antosubmit" id="findProduct">Find</button>
            </div>
        </div>
    </div>
</div>

<!-- Triggerer -->
<div hidden id="showProductsModal" data-toggle="modal" data-target="#addQuoteProductModal"></div>

<!-- Date picker -->
<script type="text/javascript">
$(document).ready(function () {
    $('#single_cal1').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        locale: {
            format: "DD/MM/YYYY"
        },
    }, function (start, end, label) {

        $("#delivery_date_value").val(start.toISOString());
    });
    $('#single_cal2').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        locale: {
            format: "DD/MM/YYYY"
        },
    }, function (start, end, label) {

        $("#expiry_date_value").val(start.toISOString());
    });
});
</script>
<!-- /Date picker -->



<!-- Autocomplete -->
<script type="text/javascript">
$(document).ready(function () {

    function highlightText (text, $node) {

        if (text == " ")
        return;

        var searchText = $.trim(text).toLowerCase(), currentNode = $node.get(0).firstChild, matchIndex, newTextNode, newSpanNode;
        while ((matchIndex = currentNode.data.toLowerCase().indexOf(searchText)) >= 0) {
            newTextNode = currentNode.splitText(matchIndex);
            currentNode = newTextNode.splitText(searchText.length);
            newSpanNode = document.createElement("span");
            newSpanNode.className = "highlight";
            currentNode.parentNode.insertBefore(newSpanNode, currentNode);
            newSpanNode.appendChild(newTextNode);
        }
    };

    var url = window.location.origin + "/products/json";
    var ProductsTableModifier = new TableModifier("#quoteProductsList", {
        warning: {
            title: "The product already exists.",
            text: "The product already exists in the quote list.",
        },
        error: {
            title: "Product not found.",
            text: "The chosen product was not found in the database.",
        },
        success: {
            title: "Product successfully added.",
            text: "The product was successfully added to the quote list.",
        }
    });

    var autocompleteArgs = {
        source: url,
        appendTo: "#autocomplete-container",
        select: function (ev, item) {}
    };

    function triggerModal() {
        $("#showProductsModal").click();
    };


    // Initialize autocomplete with custom appendTo:
    $('#pName').autocomplete(autocompleteArgs)
    .data("ui-autocomplete")._renderItem = function (ul, item) {

        var $a = $("<a></a>").css({
            "display" : "inline-block"
        }).text(item.label).css({ "width": "100%" });



        highlightText(this.term, $a);

        return $("<li class='product-item product-autocomplete-item'>").data("ui-autocomplete-item", item)
        .append(
            $a.prepend(function () {
                if(item.avatar)
                return $("<img>").attr("width", 50).attr("height", 50).attr("src", "/" + item.avatar);
                return "";
            })
            .append(function () {
                return $("<span>").text(" - Quantity: " + item.quantity + " units")
            })
        )
        .appendTo(ul);
    };

    $("#changeShipmentType").on("change", function () {
        console.log("Im here")
        var shipmentPrice = Number($(this).find(":selected").data("shipment-price"));
        $("#shipping").text(shipmentPrice);
        ProductsTableModifier.updateTotalPriceWithTaxation();
    });

    $("#addQuoteProduct").on("click", function (e) {
        e.preventDefault();

        // Show modal
        triggerModal();

    });

    $("#findProduct").on("click", function (e) {
        e.preventDefault();

        $.ajax({
            url: window.location.origin + "/products/byTerm",
            type: "get",
            data: {
                term: $("#pName").val()
            },
            success: function (quoteItem) {

                ProductsTableModifier.addRowToBody(
                    ProductsTableModifier.generateTableRow(
                        quoteItem
                    )
                );

                triggerModal();
                push_notification("Product added.", "The quote product has been succesfully added.", "success");
            },
            error: function () {
                push_notification("Product not found.", "The product was not find in the database.", "error");
            }
        });
    });
});
</script>
<!-- /Autocomplete -->
@stop
