@extends("master")

@section("content")
<div class="row">
    {{ Form::open([
        'url' => 'settings/update',
        'method' => 'POST',
        'class' => 'form-horizontal form-label-left',
        'files' => true
        ]) }}
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Settings</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Preview</a></li>
                                <li><a href="#" onclick="window.print(); return false;">Print</a></li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <section class="content invoice">

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2 col-sm-12 col-xs-12 lead">
                                    {{ Form::label('company_logo', 'Logo') }}
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    {{ Form::text('company_logo', (isset($settings["company_logo"]))?$settings["company_logo"]:"", [
                                    'class' => 'form-control',
                                    'placeholder' => 'Put your company logo',
                                    'readonly',
                                    'id'    => 'company_logo'
                                    ])
                                }}
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <a href="" class="popup_selector btn btn-md btn-info"
                                data-inputid="company_logo">Pick a file</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2 col-sm-12 col-xs-12 lead">
                                {{ Form::label('company_address', 'Address') }}
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                {{ Form::textarea('company_address', (isset($settings["company_address"]))?$settings["company_address"]:"", [
                                'class' => 'form-control',
                                'placeholder' => 'Put your company address',
                                'id'    => 'company_address',
                                'rows'  => 3
                                ])
                            }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2 col-sm-12 col-xs-12 lead">
                            {{ Form::label('company_telephone', 'Telephone/Fax') }}
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            {{ Form::textarea('company_telephone', (isset($settings["company_telephone"]))?$settings["company_telephone"]:"", [
                            'class' => 'form-control',
                            'placeholder' => 'Put your company telephone/fax',
                            'id'    => 'company_telephone',
                            'rows'  => 3
                            ])
                        }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2 col-sm-12 col-xs-12 lead">
                        {{ Form::label('company_slogan', 'Slogan') }}
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        {{ Form::text('company_slogan', (isset($settings["company_slogan"]))?$settings["company_slogan"]:"", [
                        'class' => 'form-control',
                        'placeholder' => 'Put your company slogan (optional)',
                        'id'    => 'company_slogan'
                        ])
                    }}
                </div>
            </div>
        </div>
        <div class="row">
            <input type="submit" class="btn btn-success btn-lg" value="Save" />
        </div>
    </section>
</div>
</div>
</div>
{{ Form::close() }}
</div>
@stop
