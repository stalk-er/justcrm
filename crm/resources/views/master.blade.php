<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="token" content="{{ csrf_token() }}">
    <title>CRM | </title>

    <!-- Bootstrap core CSS -->
    <script src="//maps.googleapis.com/maps/api/js"></script>
    <link href="{!! URL::asset('assets/css/colorbox/colorbox.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('assets/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('assets/fonts/css/font-awesome.min.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('assets/css/animate.min.css') !!}" rel="stylesheet">

    <!--  Calendar stylesheets -->
    <link href="{!! URL::asset('assets/css/calendar/fullcalendar.print.css') !!}" rel="stylesheet" media="print">
    <link href="{!! URL::asset('assets/css/calendar/fullcalendar.css') !!}" rel="stylesheet">

    <!-- select2 -->
    <link href="{!! URL::asset('assets/css/select/select2.min.css') !!}" rel="stylesheet">

    <!-- switchery -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/switchery/switchery.min.css') }}" />

    <!-- jQuery UI -->
    <link href="{!! URL::asset('assets/css/ui/jquery-ui.min.css') !!}" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="{!! URL::asset('assets/css/custom.css') !!}" rel="stylesheet">
    <!--        <link rel="stylesheet" type="text/css" href="{!! URL::asset('assets/css/maps/jquery-jvectormap-2.0.1.css') !!}" />-->
    <link href="{!! URL::asset('assets/css/icheck/flat/green.css') !!}" rel="stylesheet" />
    <link href="{!! URL::asset('assets/css/floatexamples.css') !!}" rel="stylesheet" type="text/css" />

    <!-- <script src="https://rawgit.com/enyo/dropzone/master/dist/dropzone.js"></script>
    <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css"> -->
    <link rel="stylesheet" href="{!! URL::asset('assets/css/ajax-loader.css') !!}">

    <script src="{!! URL::asset('assets/js/jquery.min.js') !!}"></script>
    <script src="{!! URL::asset('assets/js/index.js') !!}"></script>

    <script src="{!! URL::asset('assets/js/echart/echarts-all.js') !!}"></script>
    <script src="{!! URL::asset('assets/js/icheck/icheck.min.js') !!}"></script>
    <script src="{!! URL::asset('assets/js/cookie/jquery.cookie.js') !!}"></script>

    <!-- select2 -->
    <script src="{!! URL::asset('assets/js/select/select2.full.js') !!}"></script>

    <!-- PNotify -->
    <script type="text/javascript" src="{!! URL::asset('assets/js/notify/pnotify.core.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('assets/js/notify/pnotify.buttons.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('assets/js/notify/pnotify.nonblock.js') !!}"></script>
    <style>
        .alert.alert-dismissible { position: fixed; bottom: 0; right: 0; z-index: 999; }
        #loader-container { width: 100%; height: 100%; position: fixed; top: 0; left: 0; background-color: rgba(0,0,0,0.5); z-index: 9999; }
        #loader-container #loader { position: absolute; top: 0; left: 0; right: 0; bottom: 0; margin: auto; }
    </style>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{!! URL::asset('assets/js/moment.min.js') !!}"></script>
    <script src="{!! URL::asset('assets/js/calendar/fullcalendar.min.js') !!}"></script>
    <script type="text/javascript">
    // $(window).load(function () {
    //     //Dropzone.js Options - Upload an image via AJAX.
    //     Dropzone.options.myDropzone = {
    //         uploadMultiple: false,
    //         // previewTemplate: '',
    //         addRemoveLinks: false,
    //         // maxFiles: 1,
    //         dictDefaultMessage: '',
    //     };
    //     var token = "{{ Session::getToken() }}";
    //     var baseUrl = "{{ url('/') }}";
    //     var avatar_upload = baseUrl + '/avatar/upload';
    //     var myDropzone = new Dropzone("#change-profile-image", {
    //         url: avatar_upload,
    //         params: {
    //             _token: token
    //         },
    //         init: function () {
    //             this.on("addedfile", function (file) {
    //                 //console.log(file);
    //
    //             });
    //             this.on("thumbnail", function (file, dataUrl) {
    //                 //console.log(dataUrl);
    //                 //console.log(file);
    //             });
    //             this.on("success", function (file, response) {
    //                 //console.log(response.path);
    //                 $("#change-profile-image").attr("src", response.path);
    //             });
    //         }
    //     });
    //     $('#upload-submit').on('click', function (e) {
    //         e.preventDefault();
    //         //trigger file upload select
    //         $("#my-dropzone").trigger('click');
    //     });
    // });
    // Dropzone.autoDiscover = false;
    </script>


    <script type="text/javascript">
    //
    //            $(document).ready(function () {
    //
    //                function renderAjaxLoader(element) {
    //
    //                    element.append(
    //                            $("<div>").attr("class", "cssload-loader")
    //                            .append(
    //                                    $("<div>").attr("class", "cssload-dot")
    //                                    )
    //                            .append(
    //                                    $("<div>").attr("class", "cssload-dot")
    //                                    )
    //                            .append(
    //                                    $("<div>").attr("class", "cssload-dot")
    //                                    )
    //                            .append(
    //                                    $("<div>").attr("class", "cssload-dot")
    //                                    )
    //                            .append(
    //                                    $("<div>").attr("class", "cssload-dot")
    //                                    )
    //                            );
    //                }
    //
    //                $("#sidebar-menu a").click(function (event) {
    //                    event.preventDefault();
    //                    var href = $(this).attr("href");
    //                    var title = "CRM";
    //                    var state = href;
    //                    $.ajax({
    //                        type: "GET",
    //                        url: href,
    //                        beforeSend: function () {
    //                            renderAjaxLoader($(".right_col"));
    //                        },
    //                        success: function (data) {
    //
    //                            window.history.pushState(state, title, href);
    //                            $(".right_col").empty();
    //                            var dataObj = $(data).find(".right_col");
    //                            $(".right_col").fadeIn(500).append(dataObj.contents());
    //                        },
    //                        statusCode: {
    //                            404: function () {
    //                                $(".right_col").empty();
    //                            }
    //                        }
    //                    });
    //                });
    //            });
    </script>
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view" style="width: 100%;">
                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img id="change-profile-image" src="{!! Auth::user()->profile_image !!}" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            @if(Auth::check())
                            <span>Welcome,</span>
                            <h2 style="cursor: pointer;" onclick="window.location='{{ url('user-profile') }}'">
                                {!! Auth::user()->username !!}
                            </h2>
                            @endif
                        </div>
                    </div>
                    <!-- /menu prile quick info -->
                    <br>
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>&nbsp;</h3>
                            <ul class="nav side-menu">
                                <li><a href="{{ url('/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                <li>
                                    <a><i class="fa fa-umbrella"></i> Products <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/products/new">Add new</a></li>
                                        <li><a href="/products/list">All Products</a></li>
                                        <li><a href="/products/store">Store</a></li>
                                        <li><a href="/products/tracking">Tracking Stock</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fa fa-shopping-cart"></i> Orders <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/orders/dashboard">Dashboard</a></li>
                                        <li><a href="/orders/create">New Order</a></li>
                                        <li><a href="/orders">All Orders</a></li>
                                        <li><a href="/orders/archive">Orders archive</a></li>
                                        <li><a href="/orders/options">Order options</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fa fa-truck"></i> Vendors <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/vendors/create">Add new</a></li>
                                        <li><a href="/vendors">All Vendors</a></li>
                                        <li><a href="/vendors/contracts">Contracts</a></li>
                                        <li><a href="/vendors/statistics">Statistics</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fa fa-wrench"></i> Services <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/services/create">Add new</a></li>
                                        <li><a href="/services">All Services</a></li>
                                        <li><a href="/services/settings">Settings</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fa fa-quote-left"></i> Quotes <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/quotes/create">Add new</a></li>
                                        <li><a href="/quotes">All quotes</a></li>
                                        <li><a href="/quotes/archive">Quotes archive</a></li>
                                        <li><a href="/quotes/statistics">Statistics</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fa fa-file-archive-o"></i> Quote templates<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/quote-templates/create">Add new</a></li>
                                        <li><a href="/quote-templates">All templates</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fa fa-file-text"></i> Contracts <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/contracts/create">Add new</a></li>
                                        <li><a href="/contracts/">All contracts</a></li>
                                        <li><a href="/contracts/archive">Contracts archive</a></li>
                                        <li><a href="/contracts/options">Options</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="/event-calendar"><i class="fa fa-calendar-o"></i> Calendar </a>
                                </li>
                                <li>
                                    <a href="/events"><i class="fa fa-inbox"></i> Notifications </a>
                                <li>
                                    <a><i class="fa fa-institution"></i> Organizations <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/organizations/create">Add new</a></li>
                                        <li><a href="/organizations">All organizations</a></li>
                                        <li><a href="/organizations/options">Options</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a href="{{ URL::to('settings') }}" data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a onclick="toggleFullScreen()" data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ url('/logout') }}">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="{!! Auth::user()->profile_image !!}" alt="">
                                    @if(Auth::check())
                                    {!! Auth::user()->username !!}
                                    @endif
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href="{{ url('/user-profile') }}">Profile</a></li>
                                    <li><a href="{{ url('/settings') }}"><span>Settings</span></a></li>
                                    <li><a href="/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>

                            @include('partials.notificationMenu')

                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    @yield('content')
                </div>
                <!-- /page content -->
            </div>
        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>

        <script src="{!! URL::asset('assets/js/bootstrap.min.js') !!}"></script>


        <!-- switchery -->
        <script src="{{ URL::asset('assets/js/switchery/switchery.min.js') }}"></script>
        <!-- chart js -->
        <script src="{!! URL::asset('assets/js/chartjs/chart.min.js') !!}"></script>
        <!-- bootstrap progress js -->
        <script src="{!! URL::asset('assets/js/progressbar/bootstrap-progressbar.min.js') !!}"></script>
        <script src="{!! URL::asset('assets/js/nicescroll/jquery.nicescroll.min.js') !!}"></script>
        <!-- icheck -->
        <script src="{!! URL::asset('assets/js/icheck/icheck.min.js') !!}"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="{!! URL::asset('assets/js/moment.min.js') !!}"></script>
        <script type="text/javascript" src="{!! URL::asset('assets/js/datepicker/daterangepicker.js') !!}"></script>

        <!-- jQuery UI -->
        <script src="{!! URL::asset('assets/js/ui/jquery-ui.min.js') !!}"></script>


        <!-- Color box -->
        <script type="text/javascript" src="{{ URL::asset('/assets/js/colorbox/jquery.colorbox-min.js') }}"></script>

        <!-- Stand alone -->
        <script type="text/javascript" src="/packages/barryvdh/elfinder/js/standalonepopup.min.js"></script>


        <!-- flot js -->
        <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="{!! URL::asset('assets/js/flot/jquery.flot.js') !!}"></script>
        <script type="text/javascript" src="{!! URL::asset('assets/js/flot/jquery.flot.pie.js') !!}"></script>
        <script type="text/javascript" src="{!! URL::asset('assets/js/flot/jquery.flot.orderBars.js') !!}"></script>
        <script type="text/javascript" src="{!! URL::asset('assets/js/flot/jquery.flot.time.min.js') !!}"></script>
        <script type="text/javascript" src="{!! URL::asset('assets/js/flot/date.js') !!}"></script>
        <script type="text/javascript" src="{!! URL::asset('assets/js/flot/jquery.flot.spline.js') !!}"></script>
        <script type="text/javascript" src="{!! URL::asset('assets/js/flot/jquery.flot.stack.js') !!}"></script>
        <script type="text/javascript" src="{!! URL::asset('assets/js/flot/curvedLines.js') !!}"></script>
        <script type="text/javascript" src="{!! URL::asset('assets/js/flot/jquery.flot.resize.js') !!}"></script>
        <script>
        $(document).ready(function () {
            // [17, 74, 6, 39, 20, 85, 7]
            //[82, 23, 66, 9, 99, 6, 2]
            var data1 = [[gd(2012, 1, 1), 17], [gd(2012, 1, 2), 74], [gd(2012, 1, 3), 6], [gd(2012, 1, 4), 39], [gd(2012, 1, 5), 20], [gd(2012, 1, 6), 85], [gd(2012, 1, 7), 7]];
            var data2 = [[gd(2012, 1, 1), 82], [gd(2012, 1, 2), 23], [gd(2012, 1, 3), 66], [gd(2012, 1, 4), 9], [gd(2012, 1, 5), 119], [gd(2012, 1, 6), 6], [gd(2012, 1, 7), 9]];
            $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
                data1, data2
            ], {
                series: {
                    lines: {
                        show: false,
                        fill: true
                    },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    },
                    points: {
                        radius: 0,
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {
                    verticalLines: true,
                    hoverable: true,
                    clickable: true,
                    tickColor: "#d5d5d5",
                    borderWidth: 1,
                    color: '#fff'
                },
                colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
                xaxis: {
                    tickColor: "rgba(51, 51, 51, 0.06)",
                    mode: "time",
                    tickSize: [1, "day"],
                    //tickLength: 10,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 10
                    //mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
                },
                yaxis: {
                    ticks: 8,
                    tickColor: "rgba(51, 51, 51, 0.06)",
                },
                tooltip: false
            });
            function gd(year, month, day) {
                return new Date(year, month - 1, day).getTime();
            }
        });
        </script>

        <!-- skycons -->
        <script src="{!! URL::asset('assets/js/skycons/skycons.js') !!}"></script>
        <script>
        var icons = new Skycons({
            "color": "#73879C"
        }),
        list = [
            "clear-day", "clear-night", "partly-cloudy-day",
            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
            "fog"
        ],
        i;
        for (i = list.length; i--; )
        icons.set(list[i], list[i]);
        icons.play();</script>

        <!-- /dashbord linegraph -->
        <!-- datepicker -->
        <script type="text/javascript">
        $(document).ready(function () {

            var cb = function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
            }

            var optionSet1 = {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2015',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Clear',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            };
            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            $('#reportrange').daterangepicker(optionSet1, cb);
            $('#reportrange').on('show.daterangepicker', function () {
                console.log("show event fired");
            });
            $('#reportrange').on('hide.daterangepicker', function () {
                console.log("hide event fired");
            });
            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
            });
            $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                console.log("cancel event fired");
            });
            $('#options1').click(function () {
                $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
            });
            $('#options2').click(function () {
                $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
            });
            $('#destroy').click(function () {
                $('#reportrange').data('daterangepicker').remove();
            });
        });</script>
        <!-- gauge js -->
        <script type="text/javascript" src="{!! URL::asset('assets/js/gauge/gauge.min.js') !!}"></script>
        <script src="{!! URL::asset('assets/js/custom.js') !!}"></script>
        <script src="{!! URL::asset('assets/js/nprogress.js') !!}"></script>

        <script>NProgress.start();</script>
        <script>NProgress.done();</script>

        <!-- /datepicker -->
        <!-- /footer content -->

        <!-- Barcode reader script -->
        <script type="text/javascript">
        $(document).ready(function () {

            var keyupFiredCount = 0;

            function DelayExecution(f, delay) {
                var timer = null;
                return function () {
                    var context = this, args = arguments;

                    clearTimeout(timer);
                    timer = window.setTimeout(function () {
                        f.apply(context, args);
                    },
                    delay || 300);
                };
            }

            $.fn.ConvertToBarcodeTextbox = function () {

                $(this).focus(function () { $(this).select();
                    //    $("#msg").html("");
                });

                $(this).keydown(function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);

                    if ($(this).val() == '') {
                        keyupFiredCount = 0;
                    }
                    if (keycode == 13) { //enter key
                        $(".nextcontrol").focus();
                        return false;
                        event.stopPropagation();
                    }
                });

                $(this).keyup(DelayExecution(function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    keyupFiredCount = keyupFiredCount + 1;
                }));

                $(this).blur(function (event) {

                    if ($(this).val() == '')
                    return false;

                    if(keyupFiredCount <= 1) // Input from barcode scanner
                    {}
                    else {}

                    keyupFiredCount = 0;
                });
            };

            try {
                $("input[name='product-barcode']").ConvertToBarcodeTextbox();
                if ( $("input[name='product-barcode']") == '' )
                $("input[name='product-barcode']").focus();
            } catch (e) { }

            $(window).on("focus load", function () {
                $("input[name='product-barcode']").focus();
            });


        });
        </script>
        <!-- /Barcode reader script -->


    </body>

    </html>
