<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CRM</title>
        <!-- Bootstrap core CSS -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="../assets/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="../assets/css/animate.min.css" rel="stylesheet">
        <!-- Custom styling plus plugins -->
        <link href="../assets/css/custom.css" rel="stylesheet">
        <link href="../assets/css/icheck/flat/green.css" rel="stylesheet">
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/index.js"></script>
        <!--[if lt IE 9]>
            <script src="../assets/js/ie8-responsive-file-warning.js"></script>
            <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>
    <body style="background:#F7F7F7;" onload="disappearSlow();">
        <div class="">
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>
            <div id="wrapper">
                <div id="login" class="animate form message">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <section class="login_content">
                        <form method="POST" action="/login">
                            <h1>Login Form</h1>
                            <div>
                                <input type="text" name="login-username" class="form-control" placeholder="Username" required="" />
                            </div>
                            <div>
                                <input type="password" name="login-password" class="form-control" placeholder="Password" required="" />
                            </div>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div>
                                <input class="btn btn-default submit" type="submit" value="Log in" />
                                <a class="reset_pass" href="#">Lost your password?</a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="separator">

                                <p class="change_link">New to site?
                                    <a href="#toregister" class="to_register"> Create Account </a>
                                </p>
                                <div class="clearfix"></div>
                                <br />
                                <div>
                                    <h1><i class="fa fa-paw" style="font-size: 26px;"></i> CRM</h1>
                                    <p>©2015 All Rights Reserved. CRM </p>
                                </div>
                            </div>
                        </form>
                        <!-- form -->
                    </section>
                    <!-- content -->
                </div>
                <div id="register" class="animate form">
                    <section class="login_content">
                       
                        <form method="POST" action="/register">
                            <h1>Create Account</h1>
                            <div>
                                <input type="text" name="register-username" class="form-control" placeholder="Username" required="" />
                            </div>
                            <div>
                                <input type="email" name="register-email" class="form-control" placeholder="Email" required="" />
                            </div>
                            <div>
                                <input type="password" name="register-password" class="form-control" placeholder="Password" required="" />
                            </div>
                            <div>
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" required="" />
                            </div>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div>
                                <input class="btn btn-default submit" type="submit" value="Submit" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="separator">

                                <p class="change_link">Already a member ?
                                    <a href="#tologin" class="to_register"> Log in </a>
                                </p>
                                <div class="clearfix"></div>
                                <br />
                                <div>
                                    <h1><i class="fa fa-paw" style="font-size: 26px;"></i> CRM </h1>

                                    <p>©2015 All Rights Reserved. CRM </p>
                                </div>
                            </div>
                        </form>
                        <!-- form -->
                    </section>
                    <!-- content -->
                </div>
            </div>
        </div>
    </body>
</html>