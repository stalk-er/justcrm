@extends("master")

@section("content")
<div class="row">
    <form class="form-horizontal form-label-left">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a></li>
                                <li><a href="#">Settings 2</a></li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="col-md-12">
                        <div class="x_title">
                            <h2>Store an existing product</h2>
                            <div class="clearfix"></div>
                        </div>
                        <br>

                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" class="form-control" placeholder="Barcode" name="search-product-barcode" value="">
                                <p>&nbsp;</p>
                                <button class="btn btn-default btn-lg" id="find-product">Find product</button>
                            </div>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                    <div class="col-md-6">
                        <div class="x_title">
                            <h2>Product information</h2>
                            <div class="clearfix"></div>
                        </div>
                        <br />
                        <div class="form-group">
                            <div class="controls">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Delivery date</label>

                                <div class="col-md-9 xdisplay_inputx form-group has-feedback">
                                    <input type="text" name="product-delivery-date" class="form-control has-feedback-left" value="" id="single_cal1" placeholder="Delivery date" aria-describedby="inputSuccess2Status4">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Expiry date</label>

                                <div class="col-md-9 xdisplay_inputx form-group has-feedback">
                                    <input type="text" class="form-control has-feedback-left" name="product-expiry-date" id="single_cal2" value="" placeholder="Expiry date" aria-describedby="inputSuccess2Status4">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select id="product-status" class="select2_single form-control" name="product-status" tabindex="-1">
                                    @foreach($product_statuses as $status)
                                    <option value="{!! $status->id !!}" {!! (isset($product) && $status->id === $product->status) ? "selected" : "" !!} >{!! $status->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Lot number</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="product-lot" placeholder="Enter lot number" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">New Quantity</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="number" class="form-control" name="product-quantity" placeholder="New Quantity" value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <input type="submit" id="store" value="Store" class="btn btn-success btn-lg" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<!-- Date picker -->
<script type="text/javascript">
$(document).ready(function () {

    $('#single_cal1').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        locale: {
            format: "DD/MM/YYYY"
        },
    }, function (start, end, label) {

        $(this).val(start.toISOString());
    });

    $('#single_cal2').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        locale: {
            format: "DD/MM/YYYY"
        },
    }, function (start, end, label) {

        $(this).val(start.toISOString());
    });
});
</script>
<!-- /Date picker -->

<script type="text/javascript">
$(document).ready(function () {

    var keyupFiredCount = 0;

    function DelayExecution(f, delay) {
        var timer = null;
        return function () {
            var context = this, args = arguments;

            clearTimeout(timer);
            timer = window.setTimeout(function () {
                f.apply(context, args);
            },
            delay || 300);
        };
    }

    $.fn.ConvertToBarcodeTextbox = function () {

        $(this).focus(function () { $(this).select();
            //    $("#msg").html("");
        });

        $(this).keydown(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);

            if ($(this).val() == '') {
                keyupFiredCount = 0;
            }
            if (keycode == 13) { //enter key
                $(".nextcontrol").focus();
                return false;
                event.stopPropagation();
            }
        });

        $(this).keyup(DelayExecution(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            keyupFiredCount = keyupFiredCount + 1;
        }));

        $(this).blur(function (event) {

            if ($(this).val() == '')
            return false;

            if(keyupFiredCount <= 1) // Input from barcode scanner
            {
            }
            else {}

            keyupFiredCount = 0;
        });
    };

    try {
        $("input[name='search-product-barcode']").ConvertToBarcodeTextbox();
        if ( $("input[search-name='product-barcode']") == '' )
        $("input[name='search-product-barcode']").focus();
    } catch (e) { }

    $(window).on("focus load", function () {
        $("input[name='search-product-barcode']").focus();
    });

    $("#find-product").on("click", function (e) {
        e.preventDefault();

        var barcode = $("input[name='search-product-barcode']").val();

        $.ajax({
            type: "get",
            url: window.location.origin + "/products/" + barcode + "/get",
            data : {
                barcode: barcode
            },
            success: function (product) {
                if(!product.success) {
                    new PNotify({
                        title: 'No such barcode',
                        text: "No such barcode was found in the database",
                        type: 'error'
                    });
                    return false;
                }
                product = product.success;
                $("#single_cal1").val(product.delivery_date);
                $("#single_cal2").val(product.expiry_date);
                $("input[name='product-lot']").val(product.lot);
                $("#product-status").val(product.status);
                $("[name='product-quantity']").focus();

                new PNotify({
                    title: 'Product found!',
                    text: "Adjust the product fields if needed and save",
                    type: 'success'
                });
            },
            error: function (err) {
                new PNotify({
                    title: 'No such barcode',
                    text: "No such barcode was found in the database",
                    type: 'error'
                });
            }
        });
    });



    $("#store").on("click", function (e) {
        e.preventDefault();


        storeProduct();
    });



    var storeProduct = function (e) {

        var barcode = $("input[name='search-product-barcode']").val();
        var data = {
                "_token" : $("input[name='_token']").val(),
                "product-delivery-date" : $("input[name='product-delivery-date']").val(),
                "product-expiry-date" : $("input[name='product-expiry-date']").val(),
                "product-status" : $("[name='product-status']").val(),
                "product-lot" : $("input[name='product-lot']").val(),
                "product-quantity" : $("input[name='product-quantity']").val(),
                "product-barcode" : barcode,
        };

        $.ajax({
            url: window.location.origin + "/products/" + barcode + "/store",
            type: "post",
            data: data,
            success: function (res) {
                new PNotify({
                    title: Object.keys(res)[0].toUpperCase(),
                    text: res.success,
                    type: Object.keys(res)[0]
                });
            },
            error : function(err, e) {
                new PNotify({
                    title: 'Something went wrong',
                    text: "Could not store the product.",
                    type: 'error'
                });
            }
        });
    };
});
</script>
@stop
