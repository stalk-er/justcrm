@extends("master")

@section("content")
<div class="row">
    <form class="form-horizontal form-label-left" action="{!! isset($product) ? URL::to('products/'.$product->id.'/edit/') : URL::to('products/new/') !!}" method="POST">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>New product</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a></li>
                                <li><a href="#">Settings 2</a></li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="col-md-6">
                        <div class="x_title">
                            <h2>Product base information <small> name, vendor, prices</small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Product name</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" placeholder="Enter name" name="product-name" value="{!! (isset($product)) ? $product->name : '' !!}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Product description <span class="required">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea class="form-control" rows="3" placeholder='Enter description' name="product-description">{!! (isset($product)) ? $product->description : "" !!}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Price for end user</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" placeholder="Enter price" name="product-endprice" value="{!! (isset($product)) ? $product->end_price : '' !!}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Price for businesses</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" placeholder="Enter price" name="product-b2bprice" value="{!! (isset($product)) ? $product->b2b_price : '' !!}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Vendor price</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" placeholder="Enter price" name="product-vendor-price" value="{!! (isset($product)) ? $product->vendor_price : '' !!}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Barcode</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" placeholder="Barcode" name="product-barcode" value="{!! (isset($product)) ? $product->barcode : '' !!}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Vendor</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select class="select2_single form-control" tabindex="-1" name="product-vendor">
                                    @foreach($vendors as $vendor)
                                    <option value="{!! $vendor->id !!}" {!! (isset($product) && $product->vendor === $vendor->id) ? "selected" : "" !!} >{!! $vendor->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="x_title">
                            <h2>Events <small> delivery date, status, expiry date, lot number</small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <br />
                        <div class="form-group">
                            <div class="controls">
                                <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                    <input type="text" name="product-delivery-date" class="form-control has-feedback-left" value="{!! (isset($product)) ? $product->delivery_date : '' !!}" id="single_cal1" placeholder="Delivery date" aria-describedby="inputSuccess2Status4">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                    <input type="text" class="form-control has-feedback-left" name="product-expiry-date" id="single_cal2" value="{!! (isset($product)) ? $product->expiry_date : '' !!}" placeholder="Expiry date" aria-describedby="inputSuccess2Status4">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select class="select2_single form-control" name="product-status" tabindex="-1">
                                    @foreach($product_statuses as $status)
                                    <option value="{!! $status->id !!}" {!! (isset($product) && $status->id === $product->status) ? "selected" : "" !!} >{!! $status->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Quantity</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="number" class="form-control" name="product-quantity" placeholder="Enter quantity" value="{!! (isset($product)) ? $product->quantity : '' !!}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Lot number</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="product-lot" placeholder="Enter lot number" value="{!! (isset($product)) ? $product->lot : '' !!}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Image</label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input class="form-control" type="text" id="product-image" readonly name="avatar" value="{{ (isset($product))?$product->avatar:'' }}">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <a href="" class="popup_selector btn btn-sm btn-info" data-inputid="product-image">Pick a file</a>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12" style="height: 150px; width: 150px; overflow: hidden;">
                                @if(isset($product))
                                <img style="max-width: 100%; width: auto; height: auto;" id="productImage" src="/{{ $product->avatar }}" alt="{{ $product->name }}">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <a href="/products/list" class="btn btn-primary">Cancel</a>
                            <input type="submit" value="{!! (isset($product)) ? 'Edit' : 'Create' !!}" class="btn btn-success" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<input type="hidden" id="triggerHelper" />

<!-- Date picker -->
<script type="text/javascript">



(function () {
    $("#product-image").on("change", function (e) {



    });
})();

$(document).ready(function () {
    $('#single_cal1').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        locale: {
            format: "DD/MM/YYYY"
        },
    }, function (start, end, label) {

        $("#delivery_date_value").val(start.toISOString());
    });
    $('#single_cal2').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        locale: {
            format: "DD/MM/YYYY"
        },
    }, function (start, end, label) {

        $("#expiry_date_value").val(start.toISOString());
    });
});
</script>
<!-- /Date picker -->
@stop
