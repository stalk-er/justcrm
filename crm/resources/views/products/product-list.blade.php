@extends('master')
@section('content')

<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Products <small>list</small></h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Products</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a></li>
                                <li><a href="#">Settings 2</a></li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped products">
                        <thead>
                            <tr>
                                <th style="width: 1%">#</th>
                                <th style="width: 20%">Product Name</th>
                                <th>Product Photo</th>
                                <th>Product Status</th>
                                <th style="width: 20%">#Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                            <tr>
                                <td>#</td>
                                <td>
                                    <a>{!! $product->name !!}</a>
                                    <br />
                                    <small>{!! $product->created_at !!}</small>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <li>
                                            <img src="/{{ $product->avatar }}" class="avatar" alt="Avatar">
                                        </li>
                                    </ul>
                                </td>
                                <td>
                                    @if($product->quantity <= 0)
                                    <button type="button" data-toggle="tooltip" data-placement="top" title="Remaining quantity: 0 units" class="btn btn-danger btn-lg">Out Of Stock</button>
                                    @else
                                    <button type="button" data-toggle="tooltip" data-placement="top" title="Remaining quantity: {!! $product->quantity !!} units" class="btn btn-success btn-lg">In Stock</button>
                                    @endif
                                </td>
                                <td>
                                    <a href="/products/{!! $product->id !!}/preview" data-id="{!! $product->id !!}" class="btn btn-primary btn-xs preview"><i class="fa fa-folder"></i> View </a>
                                    <a href="/products/{!! $product->id !!}/edit" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                    <a href="/products/{!! $product->id !!}/delete" data-id="{!! $product->id !!}" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i> Delete </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="product-preview"  tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Product quick preview</h4>
            </div>
            <div class="modal-body">
                <div class="product-name"></div>
                <div class="product-description"></div>
                <div class="product-endprice"></div>
                <div class="product-vendor-price"></div>
                <div class="product-b2bprice"></div>
                <div class="product-vendor"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<script type="text/javascript">
    $(document).ready(function () {

        $('.delete').click(function (ev) {
            ev.preventDefault();

            if(confirm("Are you sure you want to delete this product?")) {

                var thisRow = $(this).parents("tr");
                var token = "{{ Session::getToken() }}";
                var url = $(this).attr("href");
                var productId = parseInt($(this).data("id"));

                $.ajax({
                    //headers: { 'csrftoken' : token },
                    type: "POST",
                    data: {
                        id: productId,
                        _token: token
                    },
                    url: url,
                    success: function (data) {
                        thisRow.remove();
                    }
                });
            }
        });




        $(".preview").click(function (ev) {
            ev.preventDefault();

            var token = "{{ Session::getToken() }}";
            var url = $(this).attr("href");
            var productId = parseInt($(this).data("id"));

            $.ajax({
                type: "POST",
                data: {
                    id: productId,
                    _token: token
                },
                url: url,
                success: function (product) {

                    console.log(product);
                    $("#product-preview").modal();
                    $("#product-preview").find(".product-name").text("Product name: " + product.name);
                    $("#product-preview").find(".product-description").text("Product description: " + product.description);
                    $("#product-preview").find(".product-endprice").text("End price: " + product.end_price);
                    $("#product-preview").find(".product-b2bprice").text("Business price: " + product.b2b_price);
                    $("#product-preview").find(".product-vendor-price").text("Vendor price: " + product.vendor_price);
                    $("#product-preview").find(".product-vendor").text("Vendor :" + product.vendor);

                    $('#product-preview').on('shown.bs.modal', function () {
                        //$('#myInput').focus()
                    })

                }
            });
        });





    });
</script>



@stop
