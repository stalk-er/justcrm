<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('name');
            $table->longText('content')->nullable();
            $table->string('start_date');
            $table->string('end_date');
            $table->string('client');
            $table->integer('template')->unsigned();
            $table->foreign('template')->references('id')->on('contract_templates')->onDelete('cascade');
            $table->string('created_by');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contracts');
    }
}
