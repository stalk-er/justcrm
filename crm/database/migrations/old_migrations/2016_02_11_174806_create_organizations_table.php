<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        Schema::create('organizations', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('name');
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('email')->nullable();
            $table->integer('telephone')->nullable();
            $table->string('website')->nullable();
            $table->string('address')->nullable();
            $table->float('location_lon')->nullable();
            $table->float('location_lat')->nullable();
            $table->string('member_of')->nullable();
            $table->integer('employees');
            $table->float('annual_revenue');
            $table->integer('type')->unsigned();
            $table->foreign('type')->references('id')->on('organization_types')->onDelete('cascade');
            $table->longText('description');
            $table->string('rating')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        
        Schema::drop('organizations');
    }

}
