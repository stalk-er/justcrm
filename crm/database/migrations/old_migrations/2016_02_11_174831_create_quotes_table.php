<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up() {
        
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id');
            $table->longText('name');
            $table->string('content')->nullable();
            $table->string('client');
            $table->integer('template')->unsigned();
            $table->foreign('template')->references('id')->on('quote_templates')->onDelete('cascade');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('quotes');
    }

}
