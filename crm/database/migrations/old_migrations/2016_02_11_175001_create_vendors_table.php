<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->string('city');
            $table->string('country');
            $table->string('address')->nullable();
            $table->integer('zip_code');
            $table->string('email');
            $table->integer('telephone');
            $table->integer('fax');
            $table->string('website')->nullable();
            $table->float('location_lon');
            $table->float('location_lat');
            $table->string('avatar_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('vendors');
    }

}
