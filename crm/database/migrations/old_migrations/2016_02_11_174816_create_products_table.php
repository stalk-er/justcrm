<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('name');
            $table->string('avatar');
            $table->longText('description')->nullable();
            $table->float('end_price')->nullable();
            $table->float('b2b_price')->nullable();
            $table->float('vendor_price')->nullable();
            $table->integer('vendor')->unsigned();
            $table->foreign('vendor')->references('id')->on('vendors')->onDelete('cascade');
            $table->integer('status')->unsigned()->nullable();
            $table->foreign('status')->references('id')->on('product_statuses')->onDelete('cascade');
            $table->integer('quantity')->nullable();
            $table->string('lot')->nullable();
            $table->string('delivery_date')->nullable();
            $table->string('expiry_date')->nullable();
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
