<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unique_id');
            $table->string('description');
            $table->integer('shipment_types')->unsigned()->nullable();
            $table->foreign('shipment_types')->references('id')->on('shipment_types')->onDelete('cascade');
            $table->integer('payment_types')->unsigned()->nullable();
            $table->foreign('payment_types')->references('id')->on('payment_types')->onDelete('cascade');
            $table->string('address')->nullable();
            $table->string('client')->nullable();
            $table->integer('status')->unsigned()->nullable();
            $table->foreign('status')->references('id')->on('order_statuses')->onDelete('cascade');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('orders');
    }

}
